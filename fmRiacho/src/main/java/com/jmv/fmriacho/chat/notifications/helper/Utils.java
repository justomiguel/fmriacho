package com.jmv.fmriacho.chat.notifications.helper;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.jmv.fmriacho.Constants;
import com.jmv.radiosantamarina.MainActivity;
import com.jmv.fmriacho.R;
import com.jmv.fmriacho.chat.notifications.ParseChatMessage;
import com.jmv.fmriacho.chat.notifications.activities.NotificationsActivity;
import com.jmv.fmriacho.chat.notifications.activities.StandardImageProgrammatic;
import com.parse.ParseFile;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cn.trinea.android.common.service.impl.ImageSDCardCache;
import cn.trinea.android.common.util.CacheManager;

public class Utils {

    public static final String LAT1 = "lat1";
    public static final String LAT2 = "lat2";
    public static final String LONG1 = "long1";
    public static final String LONG2 = "long2";

    public static final int GPS_NOT_TURNED_FF = 3;
    public static final String La_Academia = "com.jmv.fmriacho";
    public static final String APP_DOMAIN = "fmriacho";
    public static final String ONCALLMONITOR = "/"+Utils.APP_DOMAIN;
    public static final String ONCALLMONITOR_IMGS = "/"+Utils.APP_DOMAIN+"/imgs";
    public static final String DD_M_MYYYY = "ddMMyyyy";
    public static final String ALERT = "alert";
    public static final String SENDER_ID = "senderId";
    public static final String DATE_SENT = "dateSent";
    public static final String PHOTO_ID = "photoID";
    public static final String PHOTO_FILE_NAME = "photoFileName";
    public static final String FACEBOOK_ID = "facebookId";
    public static final String SPACE = " ";
    public static final String BLANK = "";
    public static final String SIMPLE_DATE_FORMAT = "SimpleDateFormat";
    public static final String YYYY = "yyyy";
    public static final String EEEE_DD_DE_MMMM_YYYY = "EEEE dd 'de' MMMM yyyy";
    public static final String es_ES = "es";
    public static final String ES = "ES";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FIRST_NOTIFICATION = "firstNotification";
    public static final String ON_CALL_MONITOR = "FM Riacho";
    public static final String WELCOME_TO_THE_NOTIFICATIONS_SYSTEM = "Welcome to the notifications system!!";
    public static final String PNG = ".png";
    public static final String LAST_CHANNEL_RECEIVED = "lastChannelReceived";
    public static final String CHANNEL = "channel";
    public static final String NEW_MESSAGE_FROM = "Mensaje nuevo ";
    public static final String USERCHANNEL = "userchannel";
    public static final String CHATS = "/chats";

    public static String EMPRESA_SELECTED = "empresa_selected";
    public static String CURRENT_ESCULTURA = "escultura_actual";
    public static final int GPS_NOT_TURNED_ON = 2;
    public static final String LAST_NID = "last_nid";

    public static String mapContents;
    public static final int CONFIRMA_COMPRA = 6;

    public static final String PREFS_LOGIN_USERNAME_KEY = "__USERNAME__";

    public static final String PREFS_LOGIN_WITH_PARSE_OK = "OK_PARSE";

    public static final String PREFS_LOGIN_IMAGE = "__IMG__";
    public static final String PREFS_LOGIN_PASSWORD_KEY = "__PASSWORD__";

    public static final String PREFS_LOGIN_PASSWORD_USER = "__NAME__";
    public static final SimpleDateFormat DATE_HOUR_FORMATTER = new SimpleDateFormat(
            "mm:ss", Locale.ENGLISH);



    public static Bitmap getRefelection(Bitmap image) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1) {

            // The gap we want between the reflection and the original image
            final int reflectionGap = 0;

            // Get your bitmap from drawable folder
            Bitmap originalImage = image;

            int width = originalImage.getWidth();
            int height = originalImage.getHeight();

            // This will not scale but will flip on the Y axis
            Matrix matrix = new Matrix();
            matrix.preScale(1, -1);

			/*
			 * Create a Bitmap with the flip matix applied to it. We only want
			 * the bottom half of the image
			 */

            Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                    height / 2, width, height / 2, matrix, false);

            // Create a new bitmap with same width but taller to fit reflection
            Bitmap bitmapWithReflection = Bitmap.createBitmap(width,
                    (height + height / 2), Bitmap.Config.ARGB_8888);
            // Create a new Canvas with the bitmap that's big enough for
            // the image plus gap plus reflection
            Canvas canvas = new Canvas(bitmapWithReflection);
            // Draw in the original image
            canvas.drawBitmap(originalImage, 0, 0, null);
            // Draw the reflection Image
            canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);

            // Create a shader that is a linear gradient that covers the
            // reflection
            Paint paint = new Paint();
            LinearGradient shader = new LinearGradient(0,
                    originalImage.getHeight(), 0,
                    bitmapWithReflection.getHeight() + reflectionGap,
                    0x99ffffff, 0x00ffffff, Shader.TileMode.CLAMP);
            // Set the paint to use this shader (linear gradient)
            paint.setShader(shader);
            // Set the Transfer mode to be porter duff and destination in
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            // Draw a rectangle using the paint with our linear gradient
            canvas.drawRect(0, height, width, bitmapWithReflection.getHeight()
                    + reflectionGap, paint);
            if (originalImage != null && originalImage.isRecycled()) {
                originalImage.recycle();
                originalImage = null;
            }
            if (reflectionImage != null && reflectionImage.isRecycled()) {
                reflectionImage.recycle();
                reflectionImage = null;
            }
            return bitmapWithReflection;
        } else {
            return null;
        }

    }

    public static void shareNovedad(Activity activity) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Estoy comprando en FM Riacho!! Venis? #FM Riacho #Corrientes";
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT,
                "FM Riacho App!");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent,
                "Compartilo en..."));
    }

    public static Uri getImageUri(Context inContext, Bitmap image) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                image, "Title", null);
        return Uri.parse(path);
    }

    public static String toDecimalFormat(Double d) {
        return new DecimalFormat("##.##").format(d);
    }

    public static long getDaysCountFromLastUpdate(String lastUpdate) {
        SimpleDateFormat formatter = new SimpleDateFormat(DD_M_MYYYY,
                Locale.getDefault());
        Date startDate;
        try {
            startDate = formatter.parse(lastUpdate);
        } catch (ParseException e) {
            return 1;
        }
        Date today = new Date();

        long startTime = startDate.getTime();
        long endTime = today.getTime();

        long diffTime = endTime - startTime;

        long diffDays = diffTime / (1000 * 60 * 60 * 24);

        return diffDays;
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static String getDateAsString() {
        SimpleDateFormat formatter = new SimpleDateFormat(DD_M_MYYYY,
                Locale.getDefault());
        return formatter.format(new Date());
    }

    public static void addTouchEffectoToButtons(View container) {
        ArrayList<View> touchables = container.getTouchables();

        final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

        for (View view : touchables) {
            if (view instanceof Button) {
                view.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                v.startAnimation(buttonClick);
                                Drawable drw = v.getBackground();
                                if (drw != null) {
                                    drw.setColorFilter(0xe0f47521,
                                            PorterDuff.Mode.SRC_ATOP);
                                }
                                v.invalidate();
                                break;
                            }
                            case MotionEvent.ACTION_UP: {
                                Drawable drw = v.getBackground();
                                if (drw != null) {
                                    drw.clearColorFilter();
                                }
                                v.invalidate();
                                break;
                            }
                        }
                        return false;
                    }

                });
            }
        }
    }



    public static ArrayList<ParseChatMessage> getMessages(Context context,
                                                          String channel) {
        ArrayList<ParseChatMessage> list = new ArrayList<ParseChatMessage>();

        try {

            // Find the directory for the SD Card using the API
            // *Don't* hardcode "/sdcard"
            File sdcard = Environment.getExternalStorageDirectory();
            File dir = new File(sdcard.getAbsolutePath() + ONCALLMONITOR+CHATS);
            // Get the text file
            File file = new File(dir, channel);

            StringBuilder text = new StringBuilder();
            String contents = null;
            if (file.exists()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;
                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                } catch (IOException e) {
                    // You'll need to add proper error handling here
                }
                contents = text.toString();
            }

            if (contents == null || contents.isEmpty()
                    || contents.equalsIgnoreCase("error")) {
                return list;
            }

            String[] linesText = contents.split("\n");

            int size = linesText.length;
            for (int i = 0; i < size; i++) {
                try {
                    JSONObject pushData = new JSONObject(linesText[i]);
                    ParseChatMessage chat = new ParseChatMessage();
                    chat.setMessage(pushData.optString(ALERT));
                    chat.setSenderId(pushData.optString(SENDER_ID));
                    chat.setDateSent(Long.parseLong(pushData
                            .optString(DATE_SENT)));
                    chat.setPhotoId(pushData.optString(PHOTO_ID));
                    chat.setFileName(pushData.optString(PHOTO_FILE_NAME));
                    chat.setFacebookId(pushData.optString(FACEBOOK_ID));
                    list.add(chat);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
            if (size > 2000) {
                Utils.saveToFile(BLANK, channel);
            }
        } catch (Exception e) {
            return new ArrayList<ParseChatMessage>();
        }

        return list;
    }

    public static void saveToFile(String result, String fileName) {
        StorageHelper helper = new StorageHelper();
        if (helper.isExternalStorageAvailableAndWriteable()) {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + ONCALLMONITOR+CHATS);
            dir.mkdirs();
            File file = new File(dir, fileName);

            try {
                FileOutputStream f = new FileOutputStream(file);
                PrintWriter pw = new PrintWriter(f);
                pw.println(result);
                pw.flush();
                pw.close();
                f.close();
            } catch (Exception e) {
            }
        }
    }

    public static synchronized void addToFile(String result, boolean b,
                                              String channel) {
        StorageHelper helper = new StorageHelper();
        if (helper.isExternalStorageAvailableAndWriteable()) {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + ONCALLMONITOR+CHATS);
            dir.mkdirs();
            File file = new File(dir, channel);

            try {
                FileWriter pw = new FileWriter(file, b);
                pw.write(result + "\n");
                pw.flush();
                pw.close();
            } catch (Exception e) {

            }

        }
    }

    public static String toCamelCase(String s) {
        String[] parts = s.split(SPACE);
        String camelCaseString = BLANK;
        for (String part : parts) {
            if (part != null && part.trim().length() > 0)
                camelCaseString = camelCaseString + toProperCase(part);
            else
                camelCaseString = camelCaseString + part + SPACE;
        }
        return camelCaseString;
    }

    static String toProperCase(String s) {
        String temp = s.trim();
        String spaces = BLANK;
        if (temp.length() != s.length()) {
            int startCharIndex = s.charAt(temp.indexOf(0));
            spaces = s.substring(0, startCharIndex);
        }
        temp = temp.substring(0, 1).toUpperCase() + spaces
                + temp.substring(1).toLowerCase() + SPACE;
        return temp;

    }

    // Saves value to shared preferences with specified key
    public static void saveToPrefs(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @SuppressLint(SIMPLE_DATE_FORMAT)
    public static String getDateByPattern(String pattern) {
        SimpleDateFormat yearGetter = new SimpleDateFormat(YYYY);
        Date myDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(
                EEEE_DD_DE_MMMM_YYYY, new Locale(es_ES, ES));
        Date date;
        try {
            date = formatter.parse(pattern + SPACE + yearGetter.format(myDate));
        } catch (ParseException e) {
            date = myDate;
        }
        return new SimpleDateFormat(YYYY_MM_DD).format(date);
    }

    // Retrieve value from shared preferences against given key
    public static String getFromPrefs(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sharedPrefs.getString(key, null);
        // NULL is the default value if nothing found in shared preferences
        // against specified key
    }

    public static void showWelcomeMessage(Activity context, String channel) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(FIRST_NOTIFICATION, true);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                intent, 0);

        String message = "{ \"facebookId\": \"301181930029298\", \"dateSent\": \"1422064797309\", \"customChannel\": \""
                + channel
                + "\", \"senderId\": \"FM Riacho\", \"alert\": \"Wellcome to FM Riacho "
                + channel + " \" }";

        Utils.addToFile(message, false, channel);
        NotificationManager mNotifM = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(ON_CALL_MONITOR).setAutoCancel(true)
                .setContentText(WELCOME_TO_THE_NOTIFICATIONS_SYSTEM)
                .setNumber(1);

        mBuilder.setContentIntent(contentIntent);

        mNotifM.notify(1, mBuilder.build());
    }

    public static void saveToFile(String filename, Bitmap bmp, Context context) {
        saveToFileWithExtension(filename, bmp, context, true);
    }

    public static void saveToFileWithExtension(String filename, Bitmap bmp,
                                               Context context, boolean b) {
        StorageHelper helper = new StorageHelper();
        if (helper.isExternalStorageAvailableAndWriteable()) {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath() + ONCALLMONITOR_IMGS);
            dir.mkdirs();
            String extensionString = BLANK;
            if (b){
                extensionString = PNG;
            }
            File file = new File(dir, filename + extensionString);
            OutputStream fOut = null;
            try {
                fOut = new FileOutputStream(file);
                Bitmap pictureBitmap = bmp;
                pictureBitmap.compress(Bitmap.CompressFormat.PNG, 60, fOut);
                fOut.flush();
                fOut.close(); // do not forget to close the stream
                MediaStore.Images.Media.insertImage(
                        context.getContentResolver(), file.getAbsolutePath(),
                        file.getName(), file.getName());
            } catch (Exception e) {
            }
        }
    }

    public static synchronized Bitmap getImageFromFile(String filename) {
        return getImageFromFileExtension(filename, true);
    }

    public static final ImageSDCardCache IMAGE_CACHE = CacheManager.getImageSDCardCache();

    public static synchronized void getImageFromFileExtensionSD(String filename, ImageView view) {
        StorageHelper helper = new StorageHelper();
        String extensionString = BLANK;
        extensionString = PNG;

        if (helper.isExternalStorageAvailableAndWriteable()) {
            File root = Environment.getExternalStorageDirectory();
            File imgFile = new File(root.getAbsolutePath() + ONCALLMONITOR_IMGS+"/"
                    + filename + extensionString);
            if (imgFile.exists()) {
                IMAGE_CACHE.get(imgFile.getAbsolutePath(), view);
            }
        }
    }

    public static synchronized Bitmap getImageFromFileExtension(String filename, boolean extension) {
        StorageHelper helper = new StorageHelper();
        String extensionString = BLANK;
        if (extension){
            extensionString = PNG;
        }
        if (helper.isExternalStorageAvailableAndWriteable()) {
            File root = Environment.getExternalStorageDirectory();
            File imgFile = new File(root.getAbsolutePath() + ONCALLMONITOR_IMGS+"/"
                    + filename + extensionString);
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                        .getAbsolutePath());
                return myBitmap;
            }
        }
        return null;
    }

    public static boolean existImageOnDisk(String filename, boolean extension) {
        String extensionString = BLANK;
        if (extension){
            extensionString = PNG;
        }
        File root = Environment.getExternalStorageDirectory();
        File imgFile = new File(root.getAbsolutePath() + ONCALLMONITOR_IMGS+"/"
                + filename + extensionString);
        if (imgFile.exists()) {
            return true;
        }
        return false;
    }

    public static void makeNotificationForChannel(Context context,
                                                  String channel, int notificationId) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LAST_CHANNEL_RECEIVED, channel);
        editor.putBoolean(channel, true);
        editor.commit();

        boolean comesFromSystem = channel.equalsIgnoreCase(Constants.SYSTEM);

        Intent intent = new Intent(context, NotificationsActivity.class);
        intent.putExtra(FIRST_NOTIFICATION, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(CHANNEL, channel);
        PendingIntent contentIntent = PendingIntent.getActivity(context, comesFromSystem?1:0,
                intent, 0);

        NotificationManager mNotifM = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Uri alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        String cantMensajes = NEW_MESSAGE_FROM + Constants.OFFICIAL_CHANNEL;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(ON_CALL_MONITOR)
                .setAutoCancel(true)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.BLUE, 3000, 3000)
                .setSound(alarmSound)
                .setContentText(cantMensajes);

        mBuilder.setContentIntent(contentIntent);

        Notification notif = mBuilder.build();

        if (comesFromSystem){
            notif.flags = Notification.FLAG_AUTO_CANCEL|Notification.FLAG_INSISTENT;
        } else {
            notif.flags = Notification.FLAG_AUTO_CANCEL;
        }


        if (NotificationsActivity.getInstance() == null){
            mNotifM.notify(notificationId, notif);
        }
    }

    public static final String CALENDAR_ID = "CALENDAR_ID";
    public static final String CALENDAR_TITLE = "CALENDAR_TITLE";
    public static final String CALENDAR_START_DATE = "CALENDAR_START_DATE";
    public static final String CALENDAR_END_DATE = "CALENDAR_END_DATE";

    public static void createMessage(FragmentManager fragmentManager, String s, String tittle) {
        com.rey.material.app.Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                fragment.dismiss();
            }

            @Override
            public void onNeutralActionClicked(DialogFragment fragment) {
                fragment.dismiss();
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                fragment.dismiss();
            }

            @Override
            public void onDismiss(DialogFragment dialog) {
            }

        };

        ((SimpleDialog.Builder) builder).message(s)
                .title(tittle)
                .positiveAction("OK");


        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(fragmentManager, null);
    }

    public static void createMessage(final FragmentManager fragmentManager, String s, String tittle, final ParseFile theFile, final Context context) {
        com.rey.material.app.Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                fragment.dismiss();
            }

            @Override
            public void onNeutralActionClicked(DialogFragment fragment) {
                fragment.dismiss();

            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                fragment.dismiss();
                if (theFile != null){
                    StandardImageProgrammatic.showHome(context, "Incident File", theFile.getUrl());
                } else {
                    Toast.makeText(context, "There is no photos yet", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onDismiss(DialogFragment dialog) {
            }

        };

        if (theFile != null){
            ((SimpleDialog.Builder) builder).message(s == null?"No comments yet":s)
                    .title(tittle)
                    .positiveAction("OK").negativeAction("See Photo");
        } else {
            ((SimpleDialog.Builder) builder).message(s == null?"No comments yet":s)
                    .title(tittle)
                    .positiveAction("OK");
        }

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(fragmentManager, null);
    }

    public static void createMessage(FragmentManager fragmentManager, String s, SimpleDialog.Builder theBuilder) {
        com.rey.material.app.Dialog.Builder builder = null;
        builder = theBuilder;

        ((SimpleDialog.Builder) builder).message(s)
                .title(Constants.TURBO_KAT_SAYS)
                .positiveAction("OK");


        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(fragmentManager, null);
    }
/*
    public static void addToCalendar(String fecha, String materia, Activity act) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(fecha));
            Calendar untilCal = Calendar.getInstance();
            untilCal.setTime(sdf.parse(fecha));
            CalendarUtil.pushAppointmentsToCalender(act, "Examen Final:"+materia, "No te olvides de llevar todo!!!", null, cal.getTimeInMillis(), untilCal.getTimeInMillis(), true, true, false);
        } catch (ParseException e) {

        }
    }
    */

}
