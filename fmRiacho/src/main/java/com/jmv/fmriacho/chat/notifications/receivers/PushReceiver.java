package com.jmv.fmriacho.chat.notifications.receivers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.jmv.fmriacho.Constants;
import com.jmv.fmriacho.chat.notifications.activities.NotificationsActivity;
import com.jmv.fmriacho.chat.notifications.helper.Utils;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

public class PushReceiver extends ParsePushBroadcastReceiver {

	private static final String TAG = "MyCustomReceiver";
	private static final int NOTIFICATION_ID = 1;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		JSONObject pushData;
		try {
			pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
			String senderId = pushData.optString("senderId");
			String channel = pushData.optString("customChannel");
			Utils.addToFile(intent.getStringExtra("com.parse.Data"), true,
					channel);
			if (senderId.contains("#")) {
				String[] usrs = senderId.split("#");
				ParseUser user = ParseUser.getCurrentUser();
				if (user != null
						&& !user.getUsername().contains(usrs[0].trim())) {
					generateNotification(context, channel);
				}
			} else {
				generateNotification(context, channel);
			}

		} catch (JSONException e) {

		}

	}

	private void generateNotification(Context context, String channel) {
		Utils.makeNotificationForChannel(context, channel, NOTIFICATION_ID);
	}

	@Override
	protected void onPushDismiss(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onPushDismiss(context, intent);
	}

	@SuppressLint("NewApi")
	@Override
	public void onPushOpen(Context context, Intent intent) {

		ParseAnalytics.trackAppOpenedInBackground(intent);

		Class<? extends Activity> cls = getActivity(context, intent);
		Intent activityIntent;

		activityIntent = new Intent(context, NotificationsActivity.class);
		intent.putExtra("firstNotification", true);

		JSONObject pushData;
		try {
			pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
			String senderId = pushData.optString("senderId");
			String channel = pushData.optString("customChannel");
			intent.putExtra("channel", Constants.OFFICIAL_CHANNEL);
		} catch (JSONException e) {

		}

		activityIntent.putExtras(intent.getExtras());

		activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(activityIntent);

	}
}
