package com.jmv.fmriacho.chat.notifications;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.jmv.fmriacho.LaPartuza;
import com.jmv.fmriacho.chat.notifications.adapter.ChatAdapter;
import com.jmv.fmriacho.chat.notifications.helper.Utils;
import com.jmv.fmriacho.parse.MessageFile;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;

import java.util.List;

public class ParseChatMessage implements Comparable<ParseChatMessage> {

	private String message;
	private boolean isFromUser;
	private String senderId;
	private Long dateSent;
	private String facebookId;
	private String channel;
	private Bitmap bmp;
	private String photoId;
	private String fileName;
	private boolean loaded;
	private int status = 0;
	private boolean existOnDisk;
	private int loadedPercentage;

	public boolean isExistOnDisk() {
		return existOnDisk;
	}

	public void setExistOnDisk(boolean existOnDisk) {
		this.existOnDisk = existOnDisk;
	}

	public int getLoadedPercentage() {
		return loadedPercentage;
	}

	public void setLoadedPercentage(int loadedPercentage) {
		this.loadedPercentage = loadedPercentage;
	}

	public String getFileName() {
		return fileName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	private boolean send;
	private ChatAdapter sta;

	public boolean isSend() {
		return send;
	}

	public void setSend(boolean send) {
		this.send = send;
	}

	public Bitmap getBmp() {
		return bmp;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public void setBmp(Bitmap bmp) {
		this.bmp = bmp;
		if (bmp!=null){
			loaded = true;
		}
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isFromUser() {
		return isFromUser;
	}

	public void setFromUser(boolean isFromUser) {
		this.isFromUser = isFromUser;
	}

	public long getDateSent() {
		// TODO Auto-generated method stub
		return dateSent;
	}

	public void setDateSent(long l) {
		this.dateSent = l;
	}

	public void loadImage(final ChatAdapter sta) {
		this.sta = sta;
		if (fileName != null && !fileName.equals("")
				&& !getFileName().trim().contains("null")) {
			if (!Utils.existImageOnDisk(fileName, true)){
				//baseLoadImage(fileName);
				loadedPercentage = 1;
				sta.notifyDataSetChanged();
				ParseQuery<ParseObject> gameQuery = ParseQuery
						.getQuery("MessageFile");
				gameQuery.whereEqualTo("title", fileName);

				gameQuery.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> list, ParseException e) {
						if (list != null && list.size() > 0) {
							MessageFile file = (MessageFile) list.get(0);
							ParseFile photoFile = file.getParseFile("photo");
							if (photoFile != null) {
								photoFile.getDataInBackground(new GetDataCallback() {
									@Override
									public void done(byte[] bytes, ParseException e) {
										Bitmap bmpFromParse = BitmapFactory.decodeByteArray(
												bytes, 0, bytes.length);
										setBmp(bmpFromParse);
										loadedPercentage = 100;
										sta.notifyDataSetChanged();
										Utils.saveToFile(fileName, bmpFromParse, LaPartuza.app);
									}
								}, new ProgressCallback() {
									@Override
									public void done(Integer integer) {
										loadedPercentage = integer;
										sta.notifyDataSetChanged();
									}
								});
							}
						}
					}
				});
			} else {
				Bitmap bmp = Utils.getImageFromFile(fileName);
				setBmp(bmp);
				loadedPercentage = 100;
			}
		}
	}

	private void baseLoadImage(final String filename) {
		new AsyncTask<Void, Void, Bitmap>(){
			@Override
			protected void onPostExecute(Bitmap aVoid) {

			}

			@Override
			protected Bitmap doInBackground(Void... params) {
				loadedPercentage = 1;
				sta.notifyDataSetChanged();
				if (!Utils.existImageOnDisk(filename, true)) {

				} else {

					Bitmap bmp = Utils.getImageFromFile(fileName);
					setBmp(bmp);
					loadedPercentage = 100;
					sta.notifyDataSetChanged();
				}
				return bmp;
			}
		}.execute();
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	private Bitmap getImage() {

		return bmp;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{\"alert\": \"" + message + "\" ," + "\"senderId\": \""
				+ senderId + "\"," + "\"facebookId\": \"" + facebookId
				+ "\", \"customChannel\": \"" + channel + "\", \"photoID\": \""
				+ photoId + "\", \"photoFileName\": \"" + fileName
				+ "\", \"dateSent\": \"" + dateSent + "\"}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result
				+ ((senderId == null) ? 0 : senderId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParseChatMessage other = (ParseChatMessage) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (senderId == null) {
			if (other.senderId != null)
				return false;
		} else if (!senderId.equals(other.senderId))
			return false;
		return true;
	}

	@Override
	public int compareTo(ParseChatMessage another) {
		return dateSent.compareTo(another.getDateSent());
	}

}
