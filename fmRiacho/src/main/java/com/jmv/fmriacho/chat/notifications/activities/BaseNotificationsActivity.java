package com.jmv.fmriacho.chat.notifications.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jmv.fmriacho.Constants;
import com.jmv.fmriacho.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

import static com.jmv.fmriacho.Constants.COMPLETAR_ACCION_USANDO;
import static com.jmv.fmriacho.Constants.DESDE_LA_CAMARA;
import static com.jmv.fmriacho.Constants.DESDE_MIS_ARCHIVOS;
import static com.jmv.fmriacho.Constants.IMAGE_TYPE;
import static com.jmv.fmriacho.Constants.SELECCIONAR_IMAGEN;

/**
 * Created by jvargas on 5/6/15.
 */
public abstract class BaseNotificationsActivity extends ActionBarActivity {

    public static final int PICK_FROM_CAMERA = 65527;
    public static final int PICK_FROM_FILE = 65526;
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_NOSTATUS = 0;
    public static final int STATUS_ERROR = -1;


    protected PullToRefreshListView messagesContainer;
    protected ImageView sendButton;
    protected ProgressBar progressBar;
    protected ImageView button;
    protected AlertDialog dialog;
    protected String oficcialChannel = Constants.OFFICIAL_CHANNEL;
    protected int count;

    protected EmojiconEditText emojiconEditText;
    protected View mHomeFormView;
    protected View mLoginStatusView;
    protected ImageView phoneButton;
    protected ImageView emojiButton;

    public String getOficcialChannel() {
        return oficcialChannel;
    }

    public static boolean isValidEmailAddress(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    protected void createCameraFUntionalities() {
        final String[] items = new String[]{DESDE_LA_CAMARA,
                DESDE_MIS_ARCHIVOS};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(SELECCIONAR_IMAGEN);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            private Uri mImageCaptureUri;

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    try {
                        Intent intent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                    }
                    dialog.cancel();
                } else {
                    Intent intent = new Intent();
                    intent.setType(IMAGE_TYPE);
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent,
                            COMPLETAR_ACCION_USANDO), PICK_FROM_FILE);
                }
            }
        });

        dialog = builder.create();
    }

    protected void addListeners() {
       emojiButton = (ImageView) findViewById(R.id.ImageView01);
        // Give the topmost view of your activity layout hierarchy. This will be
        // used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(mHomeFormView, this);

        // Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        // Set on emojicon click listener
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                emojiconEditText.append(emojicon.getEmoji());
            }
        });

        // Set on backspace click listener
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_yahoo);
            }
        });

        // If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        // On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                emojiconEditText.append(emojicon.getEmoji());
            }
        });

        // On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // If popup is not showing => emoji keyboard is not visible, we
                // need to show it
                if (!popup.isShowing()) {

                    // If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }

                    // else, open the text keyboard first and immediately after
                    // that show the emoji popup
                    else {
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText,
                                InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }
                }

                // If popup is showing, simply dismiss it to show the undelying
                // text keyboard
                else {
                    popup.dismiss();
                }
            }
        });
    }

    protected void changeEmojiKeyboardIcon(ImageView iconToBeChanged,
                                         int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mHomeFormView.setVisibility(View.GONE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
