package com.jmv.fmriacho.chat.notifications.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.jmv.fmriacho.R;
import com.jmv.fmriacho.chat.notifications.ParseChatMessage;
import com.jmv.fmriacho.chat.notifications.activities.StandardImageProgrammatic;
import com.jmv.fmriacho.chat.notifications.helper.Utils;
import com.jmv.fmriacho.utils.TimeUtils;
import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.List;

import cn.trinea.android.common.service.impl.ImageSDCardCache;
import cn.trinea.android.common.util.CacheManager;
import github.ankushsachdeva.emojicon.EmojiconTextView;

public class ChatAdapter extends BaseAdapter {

	public static final ImageSDCardCache IMAGE_CACHE = CacheManager.getImageSDCardCache();
	public static final String THUMBNAIL = "thumbnail";
	public static final String IMAGE_URL = "http://www.filecluster.com/howto/wp-content/uploads/2014/07/User-Default.jpg";

	private final List<ParseChatMessage> chatMessages;
	private List<View> views;
	private Activity context;

	private int VIEW_TYPE_FROM_USR = 1;
	private int VIEW_TYPE_FROM_SENDER = 0;

	public ChatAdapter(Activity context, List<ParseChatMessage> chatMessages) {
		this.context = context;
		this.chatMessages = chatMessages;
	}

	@Override
	public int getCount() {
		if (chatMessages != null) {
			return chatMessages.size();
		} else {
			return 0;
		}
	}

	@Override
	public ParseChatMessage getItem(int position) {
		if (chatMessages != null) {
			return chatMessages.get(position);
		} else {
			return null;
		}
	}

	public void clear() {
		this.chatMessages.clear();
		this.notifyDataSetChanged();
	}

	public List<ParseChatMessage> getChatMessages() {
		return chatMessages;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		ParseChatMessage chatMessage = getItem(position);
		if (chatMessage.isFromUser()) {
			return VIEW_TYPE_FROM_USR;
		} else {
			return VIEW_TYPE_FROM_SENDER;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		ParseChatMessage chatMessage = getItem(position);

		int type = getItemViewType(position);

		if (type == VIEW_TYPE_FROM_SENDER){
			if (convertView == null) {
				LayoutInflater vi;
				vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = vi.inflate(R.layout.list_item_message, null);
				holder = createViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
		} else {
			if (convertView == null) {
				LayoutInflater vi;
				vi = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = vi.inflate(R.layout.list_item_message_from_user, null);
				holder = createViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
		}

		/*

		if (!chatMessage.isFromUser()) {
			holder.contentWithBG
					.setBackgroundResource(R.drawable.incoming_message_bg);
		} else {
			holder.contentWithBG
					.setBackgroundResource(R.drawable.outgoing_message_bg);
		}

		*/

		boolean isOutgoing = !chatMessage.isFromUser();

		holder.txtMessage.setText(chatMessage.getMessage());

		boolean comeFromPush = (chatMessage.getFileName() == null)
				|| chatMessage.getFileName().isEmpty()
				|| chatMessage.getFileName().trim().contains("null");
		if (chatMessage.getBmp() == null && comeFromPush) {
			holder.txtMessage.setVisibility(View.VISIBLE);
			holder.containerPictureFrame.setVisibility(View.GONE);
			holder.containerImg.setVisibility(View.GONE);
		} else {
			holder.txtMessage.setVisibility(View.GONE);
			holder.containerImg.setVisibility(View.VISIBLE);
			holder.containerPictureFrame.setVisibility(View.VISIBLE);
			loadPhoto(holder, chatMessage);
		}

		holder.txtInfo.setText(getTimeText(chatMessage));

		if (isOutgoing) {
			holder.userProfilePictureView.setProfileId(chatMessage.getFacebookId());
			String username = chatMessage.getSenderId().split("#")[1];
			holder.sender.setText(username);
			holder.sender.setVisibility(View.VISIBLE);
		} else {
			holder.sender.setVisibility(View.GONE);
			holder.sender.setText("");
			String facebookId = getFacebookId();
			holder.userProfilePictureView.setProfileId(facebookId);
		}

		if (chatMessage.getLoadedPercentage() > 0 && chatMessage.getLoadedPercentage() != 100){
			holder.waitingScreen.setVisibility(View.VISIBLE);
			float progressToFloat = Float.parseFloat(String.valueOf(chatMessage.getLoadedPercentage())) /100;
			holder.progress.setProgress(progressToFloat);
		} else if (chatMessage.getLoadedPercentage() == 0){
			holder.progress.setProgress(0f);
			holder.waitingScreen.setVisibility(View.GONE);
		} else if (chatMessage.getLoadedPercentage() == 100){
			holder.waitingScreen.setVisibility(View.GONE);
		}

		if (chatMessage.getStatus() != 0){
			if (chatMessage.getStatus() == 1){
				holder.ic_succes.setVisibility(View.VISIBLE);
			} else if (chatMessage.getStatus() == -1){
				holder.ic_failed.setVisibility(View.VISIBLE);
			}
		} else {
			holder.ic_succes.setVisibility(View.GONE);
			holder.ic_failed.setVisibility(View.GONE);
		}

		return convertView;
	}

	private String getFacebookId() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser.has("profile")) {
			JSONObject userProfile = currentUser.getJSONObject("profile");
			if (userProfile.has("facebookId")) {
				try {
					return userProfile.getString("facebookId");
				} catch (JSONException e) {
					return null;
				}
			}
		}
		return null;
	}

	private void loadPhoto(ViewHolder holder, final ParseChatMessage chat) {

		if (chat.getBmp() != null && chat.isLoaded()) {
			holder.waitingScreen.setVisibility(View.GONE);
			holder.containerImg.setImageBitmap(chat.getBmp());
			holder.containerImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageView t = (ImageView) v;
					Bitmap bitmap = t.getDrawable() != null ? ((BitmapDrawable) t
							.getDrawable()).getBitmap() : null;
					if (bitmap != null) {
						StandardImageProgrammatic.showHomeBmp(context, chat.getFileName(), bitmap);
					}

				}
			});
		} else if (chat.isExistOnDisk()){

			Utils.getImageFromFileExtensionSD(chat.getFileName(), holder.containerImg);
			holder.waitingScreen.setVisibility(View.GONE);
			holder.containerImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageView t = (ImageView) v;
					Bitmap bitmap = t.getDrawable() != null ? ((BitmapDrawable) t
							.getDrawable()).getBitmap() : null;
					if (bitmap != null) {
						StandardImageProgrammatic.showHomeBmp(context, chat.getFileName(), bitmap);
					}

				}
			});
		} else {
			holder.containerImg.setVisibility(View.GONE);
		}

	}

	public void add(ParseChatMessage message) {
		chatMessages.add(message);
	}

	public void add(Collection<ParseChatMessage> messages) {
		chatMessages.addAll(messages);
	}

	private ViewHolder createViewHolder(View v) {
		ViewHolder holder = new ViewHolder();
		holder.txtMessage = (EmojiconTextView) v.findViewById(R.id.txtMessage);
		holder.content = (LinearLayout) v.findViewById(R.id.content);
		holder.contentWithBG = (LinearLayout) v
				.findViewById(R.id.contentWithBackground);
		holder.txtInfo = (TextView) v.findViewById(R.id.txtInfo);
		holder.sender = (TextView) v.findViewById(R.id.sender);


		holder.userProfilePictureView = (com.facebook.widget.ProfilePictureView) v
				.findViewById(R.id.userProfilePicture);

		holder.ic_succes = (ImageView) v.findViewById(R.id.succes_imag);
		holder.ic_failed = (ImageView) v.findViewById(R.id.failed_imag);
		holder.containerImg = (ImageView) v.findViewById(R.id.containerImg);
		holder.waitingScreen = (FrameLayout) v.findViewById(R.id.waitingScreen);
		holder.progress = (ProgressView) v.findViewById(R.id.progress);
		holder.containerPictureFrame = (FrameLayout) v
				.findViewById(R.id.containerPictureFrame);
		holder.progress.start();
		return holder;
	}

	private String getTimeText(ParseChatMessage message) {
		return TimeUtils.millisToLongDHMS(message.getDateSent());
	}

	private static class ViewHolder {
		public FrameLayout waitingScreen;
		public FrameLayout containerPictureFrame;
		public EmojiconTextView txtMessage;
		public ProgressView progress;
		public TextView sender;
		public TextView txtInfo;
		public TextView txtInfoWrong;
		public LinearLayout content;
		public LinearLayout contentWithBG;
		public ImageView ic_succes;
		public ImageView ic_failed;
		public ImageView containerImg;
		public com.facebook.widget.ProfilePictureView userProfilePictureView;
	}

	public void showSucces() {
		// TODO Auto-generated method stub

	}
}
