package com.jmv.fmriacho.chat.notifications.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jmv.fmriacho.BaseActivity;
import com.jmv.fmriacho.Constants;
import com.jmv.fmriacho.R;
import com.jmv.fmriacho.chat.notifications.ParseChatMessage;
import com.jmv.fmriacho.chat.notifications.adapter.ChatAdapter;
import com.jmv.fmriacho.chat.notifications.helper.SessionIdentifierGenerator;
import com.jmv.fmriacho.chat.notifications.helper.Utils;
import com.jmv.fmriacho.parse.MessageFile;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.parse.ui.ParseLoginBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;

import github.ankushsachdeva.emojicon.EmojiconEditText;

import static com.jmv.fmriacho.Constants.*;
import static com.jmv.fmriacho.Constants.ALERT;
import static com.jmv.fmriacho.Constants.BLANK;
import static com.jmv.fmriacho.Constants.CHANNEL;
import static com.jmv.fmriacho.Constants.CHAT;
import static com.jmv.fmriacho.Constants.CHAT_WITH;
import static com.jmv.fmriacho.Constants.COM_PARSE_DATA;
import static com.jmv.fmriacho.Constants.COM_PARSE_PUSH_INTENT_RECEIVE;
import static com.jmv.fmriacho.Constants.CUSTOM_CHANNEL;
import static com.jmv.fmriacho.Constants.DATE_SENT;
import static com.jmv.fmriacho.Constants.FACEBOOK_ID;
import static com.jmv.fmriacho.Constants.FROM_SELECT_PAGE;
import static com.jmv.fmriacho.Constants.IMAGE_URL;
import static com.jmv.fmriacho.Constants.LAST_CHANNEL_RECEIVED;
import static com.jmv.fmriacho.Constants.LA_IMAGEN_SELECCIONA_NO_ES_VALIDA_PARA_SER_ENVIADA;
import static com.jmv.fmriacho.Constants.MAIL_SEPARATOR;
import static com.jmv.fmriacho.Constants.MESSAGES_COUNT;
import static com.jmv.fmriacho.Constants.MESSAGES_TO_SHOW;
import static com.jmv.fmriacho.Constants.NO_SE_PUDO_GUARDAR_LA_IMAGEN_RE_INTENTA;
import static com.jmv.fmriacho.Constants.NUMERO;
import static com.jmv.fmriacho.Constants.PHOTO_FILE_NAME;
import static com.jmv.fmriacho.Constants.PHOTO_ID;
import static com.jmv.fmriacho.Constants.PHOTO_PNG;
import static com.jmv.fmriacho.Constants.PREFIX;
import static com.jmv.fmriacho.Constants.SENDER_ID;
import static com.jmv.fmriacho.Constants.SEPARATOR;
import static com.jmv.fmriacho.Constants.TARGET_SEPARATOR;
import static com.jmv.fmriacho.Constants.THUMBNAIL;
import static com.jmv.fmriacho.Constants.TURBO_KAT_SAYS;
import static com.jmv.fmriacho.Constants.USERCHANNEL;
import static com.jmv.fmriacho.Constants.YYYY_M_MDD_H_HMMSS;

public class NotificationsActivity extends BaseNotificationsActivity {

    private static final int LOGIN_REQUEST = 65525;
    private static NotificationsActivity app ;
    protected LinkedHashSet<ParseChatMessage> myMessages;
    protected List<ParseChatMessage> myChatMessagesShown;

    protected volatile ChatAdapter adapter;

    private ArrayAdapter<String> adp1;

    private AdView adView;

    public void displayAd() {
        String showAd= getString(R.string.enableads);
        if (showAd.equals("yes")) {
            // Create the adView
            try {

                if (adView != null) {
                    adView.destroy();
                }

                adView = new AdView(this);
                adView.setAdSize(AdSize.SMART_BANNER);
                adView.setAdUnitId(getString(R.string.publisherID));

                // Add the AdView to the view hierarchy. The view will have no size
                // until the ad is loaded.
                LinearLayout layout = (LinearLayout) findViewById(R.id.adLayout);
                layout.addView(adView);

                // Create an ad request. Check logcat output for the hashed device ID to
                // get test ads on a physical device.
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
                        .build();

                // Start loading the ad in the background.
                adView.loadAd(adRequest);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LinearLayout layout = (LinearLayout) findViewById(R.id.adLayout);
            layout.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
            layout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mBroadcastReceiver);
        ParseInstallation.getCurrentInstallation().put("online", false);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        if (adView != null) {
            adView.destroy();
        }
    }

    final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            final ParseChatMessage chat = new ParseChatMessage();
            JSONObject pushData;
            try {
                pushData = new JSONObject(
                        intent.getStringExtra(COM_PARSE_DATA));
                String senderId = pushData.optString(SENDER_ID);
                    if (senderId.contains(SEPARATOR)) {
                        String[] usrs = senderId.split(SEPARATOR);
                        ParseUser user = ParseUser.getCurrentUser();
                        if (user != null
                                && !user.getUsername().contains(usrs[0].trim())) {
                            chat.setMessage(pushData.optString(ALERT));
                            chat.setSenderId(senderId);
                            chat.setDateSent(Long.parseLong(pushData
                                    .optString(DATE_SENT)));
                            chat.setSend(true);
                            chat.setBmp(null);
                            //chat.setStatus(1);
                            chat.setPhotoId(pushData.optString(PHOTO_ID));
                            chat.setFileName(pushData
                                    .optString(PHOTO_FILE_NAME));
                            chat.setFacebookId(pushData.optString(FACEBOOK_ID));
                            chat.setFromUser(false);

                            chat.loadImage(adapter);
                            adapter.notifyDataSetInvalidated();
                            showMessage(chat);
                            adapter.notifyDataSetChanged();

                        }
                    }

            } catch (JSONException e) {

            }
        }
    };

    public static NotificationsActivity getInstance() {
        return app;
    }


    @Override
    public void onPause() {
        app = null;
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
        //    getMenuInflater().inflate(R.menu.notifications, menu);
            return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_example) {
        //    dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FROM_CAMERA) {
            if (resultCode == RESULT_OK) {
                Bitmap bmp = (Bitmap) data.getExtras().get("data");

                publishImage(bmp);
            }
        } else if (requestCode == PICK_FROM_FILE) {
            if (resultCode == RESULT_OK) {
                final Uri imageUri = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver()
                            .openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory
                            .decodeStream(imageStream);

                    publishImage(selectedImage);
                } catch (FileNotFoundException e) {
                    Utils.createMessage(((ActionBarActivity) NotificationsActivity.this).getSupportFragmentManager(), LA_IMAGEN_SELECCIONA_NO_ES_VALIDA_PARA_SER_ENVIADA, TURBO_KAT_SAYS);
                }
            }
        }  else if (requestCode == LOGIN_REQUEST){
           // com.jmv.fmriacho.utils.Utils.showWelcomeMessage(this, "FM Riacho");
            ParsePush.subscribeInBackground(Utils.APP_DOMAIN);
            ParseInstallation.getCurrentInstallation().saveInBackground();
            ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
            ParseUser user = ParseUser.getCurrentUser();
            makeMeRequest();
        }
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(COM_PARSE_PUSH_INTENT_RECEIVE);
        filter.setPriority(2);
        this.registerReceiver(mBroadcastReceiver, filter);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(oficcialChannel, false);
        editor.commit();

        this.setTitle(CHAT );
        app = this;
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notifications);


        ParseInstallation.getCurrentInstallation().put("online", true);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        if (ParseUser.getCurrentUser() == null){
            logIn();
        } else {
            makeMeRequest();
        }

        getOnLineStatus();
    }

    public void getOnLineStatus() {
        final TextView online = (TextView) findViewById(R.id.online);
        final TextView total = (TextView) findViewById(R.id.oyentes);

        final HashMap<String, Object> params = new HashMap<String, Object>();
        ParseCloud.callFunctionInBackground("getInstallations", params, new FunctionCallback<Integer>() {
            public void done(final Integer value, ParseException e) {
                if (e == null) {
                    try {
                        int totalNUmber = value;
                        String textToPLace = getString(R.string.total_users) + " " + totalNUmber;
                        total.setText(textToPLace);
                    } catch (Exception e2) {

                    }
                }
            }
        });

        final HashMap<String, Object> params2 = new HashMap<String, Object>();

        ParseCloud.callFunctionInBackground("getOnLine", params2, new FunctionCallback<Integer>() {
            public void done(final Integer value, ParseException e) {
                if (e == null) {
                    try {
                        int totalNUmber = value;
                        String textToPLace = getString(R.string.total_online) + " " + totalNUmber;
                        online.setText(textToPLace);
                    } catch (Exception e2) {

                    }
                }
            }
        });

    }

    public void makeMeRequest() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseInstallation.getCurrentInstallation().put("user", currentUser);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        if (!currentUser.has("profile")) {
            Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
                    new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null ) {
                                // Create a JSON object to hold the profile info
                                JSONObject userProfile = new JSONObject();
                                try {
                                    // Populate the JSON object
                                    userProfile.put("facebookId", user.getId());
                                    userProfile.put("name", user.getName());
                                    if (user.getProperty("gender") != null) {
                                        userProfile.put("gender",
                                                user.getProperty("gender"));
                                    }
                                    if (user.getProperty("email") != null) {
                                        userProfile.put("email",
                                                user.getProperty("email"));
                                    }
                                    // Save the user profile info in a user property
                                    ParseUser currentUser = ParseUser
                                            .getCurrentUser();
                                    currentUser.put("profile", userProfile);
                                    currentUser.saveInBackground();
                                    // Show the user info
                                    getAllData();
                                } catch (JSONException e) {
                                    getAllData();
                                }
                            } else if (response.getError() != null) {
                                getAllData();
                            }
                            //showProgress(false);
                        }
                    });
            request.executeAsync();
        } else {
            getAllData();
        }

    }

    private void getAllData() {
        mHomeFormView = findViewById(R.id.home_form);

        mLoginStatusView = findViewById(R.id.loading_content_status);
        showProgress(true);

        final ParseUser currentUser = ParseUser.getCurrentUser();

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        String channelOnPrefs = prefs.getString(LAST_CHANNEL_RECEIVED, null);
        String channel = channelOnPrefs;

        if (getIntent().getBooleanExtra(FROM_SELECT_PAGE, false)) {
            oficcialChannel = getIntent().getStringExtra(CHANNEL);
        } else {
            oficcialChannel = channel;
        }

        addListeners();


        initViews( Constants.OFFICIAL_CHANNEL);

        createCameraFUntionalities();

        showProgress(false);

        displayAd();
    }

    public void logIn() {
        ParseLoginBuilder builder = new ParseLoginBuilder(this);
        Intent parseLoginIntent = builder
                .setParseLoginEnabled(false)
                .setParseLoginButtonText("Entrar")
                .setParseSignupButtonText("Registrarse")
                .setParseLoginHelpText("Te olvidaste de tu password?")
                .setParseLoginInvalidCredentialsToastText(
                        "El mail no es correcto")
                .setParseLoginEmailAsUsername(true)
                .setParseSignupSubmitButtonText("Enviar registracion")
                .setFacebookLoginEnabled(true)
                .setFacebookLoginButtonText("Ingresar con Facebook")
                .setTwitterLoginEnabled(false)
                .setTwitterLoginButtontext("Twitter").build();
        startActivityForResult(parseLoginIntent, LOGIN_REQUEST);
    }

    private void initViews(final String chanel) {
        messagesContainer = (PullToRefreshListView) findViewById(R.id.messagesContainer);
        emojiconEditText = (EmojiconEditText) findViewById(R.id.messageEdit);
        sendButton = (ImageView) findViewById(R.id.chatSendButton);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        adapter = new ChatAdapter(this, new ArrayList<ParseChatMessage>());
        messagesContainer.setAdapter(adapter);
        messagesContainer.setAnimationCacheEnabled(false);
        messagesContainer.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                // Do work to refresh the list here.

                getMessagesFromPush(oficcialChannel);
            }
        });



        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onSendButtonClicked(v);
            }
        });

        setTitle("Chat");
        getMessagesFromPush(chanel);
    }

    private String getFacebookId() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser.has("profile")) {
            JSONObject userProfile = currentUser.getJSONObject("profile");
            if (userProfile.has("facebookId")) {
                try {
                    return userProfile.getString("facebookId");
                } catch (JSONException e) {
                    return null;
                }
            }
        }
        return null;
    }

    private void onSendButtonClicked(View v) {
        // Reset errors.
        emojiconEditText.setError(null);

        String messageText = emojiconEditText.getText().toString();
        if (TextUtils.isEmpty(messageText)) {
            emojiconEditText
                    .setError(getString(R.string.error_field_required_notif));
            emojiconEditText.requestFocus();
            return;
        }

        ParseUser user = ParseUser.getCurrentUser();

        // Send chat message
        //
        final ParseChatMessage chatMessage = new ParseChatMessage();
        chatMessage.setMessage(messageText);
        chatMessage.setFromUser(true);
        chatMessage.setFacebookId(getFacebookId());
        chatMessage.setDateSent(new Date().getTime());
        chatMessage.setChannel(oficcialChannel);

        String username = ParseUser.getCurrentUser().getString("name");

        chatMessage.setSenderId(user.getUsername() + SEPARATOR
                + username);

        JSONObject data = null;
        try {
            data = new JSONObject(chatMessage.toString());
            ParseUser userFromApp = ParseUser.getCurrentUser();
            long count = 1;
            if (userFromApp.containsKey(MESSAGES_COUNT)) {
                userFromApp.increment(MESSAGES_COUNT);
                count = userFromApp.getInt(MESSAGES_COUNT);
            } else {
                userFromApp.put(MESSAGES_COUNT, 1);
            }

            userFromApp.saveInBackground();


            if (Utils.isNetworkAvailable(NotificationsActivity.this)) {
                // Send push notification to query
                Utils.addToFile(data.toString(), true,
                        OFFICIAL_CHANNEL);
                ParsePush push = new ParsePush();
                // push.setQuery(pq);
                push.setChannel(OFFICIAL_CHANNEL);
                push.setData(data);
                chatMessage.setStatus(STATUS_NOSTATUS);

                push.sendInBackground(new SendCallback() {

                    @Override
                    public void done(ParseException arg0) {
                        if (arg0 == null) {
                            chatMessage.setStatus(STATUS_SUCCESS);
                        } else {
                            chatMessage.setStatus(STATUS_ERROR);
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
            } else {
                chatMessage.setStatus(STATUS_ERROR);
                adapter.notifyDataSetChanged();
            }


            emojiconEditText.setText(BLANK);

            showMessage(chatMessage);
        } catch (Exception e) {
           Toast.makeText(this, "Se produjo un error al enviar el mensaje", Toast.LENGTH_SHORT).show();
        }



    }

    private void getMessagesFromPush(String chanel) {
        //adapter.clear();
        //showProgress(true);
        new LongOperation().execute(chanel);

    }

    private class LongOperation extends AsyncTask<String, Void, String> {


        public LongOperation() {
        }

        @Override
        protected String doInBackground(String... params) {
            if (myMessages == null) {
                ArrayList<ParseChatMessage> messages = Utils.getMessages(
                        NotificationsActivity.this, params[0]);
                count = 0;
                myMessages = new LinkedHashSet<ParseChatMessage>(
                        messages);
                myChatMessagesShown = new ArrayList<ParseChatMessage>();
            }
            int size = myMessages.size();
            Object[] parseChatMessages = myMessages.toArray();

            int countInternal = 0;

            for (int i = size - count - 1; i > -1; i--) {
                final ParseChatMessage parseChatMessage = (ParseChatMessage) parseChatMessages[i];
                parseChatMessage.setFromUser(false);
                parseChatMessage.setStatus(STATUS_SUCCESS);
                if (parseChatMessage.getSenderId().contains(SEPARATOR)) {
                    String[] usrs = parseChatMessage.getSenderId().split(SEPARATOR);
                    ParseUser user = ParseUser.getCurrentUser();
                    parseChatMessage.setSend(true);
                    parseChatMessage.loadImage(adapter);
                    if (user != null && user.getUsername() != null && usrs[0] != null && user.getUsername().contains(usrs[0].trim())) {
                        parseChatMessage.setFromUser(true);
                    }
                }
                myChatMessagesShown.add(0, parseChatMessage);

                countInternal++;
                if (countInternal == MESSAGES_TO_SHOW) {
                    break;
                }
                //showMessage(parseChatMessage);
            }
            count += MESSAGES_TO_SHOW;
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            adapter.clear();
            adapter.add(myChatMessagesShown);
            adapter.notifyDataSetChanged();
            //scrollDown();
            messagesContainer.onRefreshComplete();
            //showProgress(false);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void showMessage(ParseChatMessage message) {
        adapter.add(message);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                scrollDown();
            }
        });
    }

    private void scrollDown() {
        //messagesContainer.setSelection(messagesContainer.getChildCount() - 1);
    }

    public void publishImage(Bitmap bitmap) {
        ParseChatMessage chatMessage = new ParseChatMessage();
        chatMessage.setMessage(BLANK);
        chatMessage.setFromUser(true);
        chatMessage.setDateSent(new Date().getTime());
        String timeStamp = new SimpleDateFormat(YYYY_M_MDD_H_HMMSS)
                .format(new Date());
        chatMessage.setFileName(PREFIX
                + new SessionIdentifierGenerator().nextSessionId());
        chatMessage.setChannel(oficcialChannel);
        ParseUser user = ParseUser.getCurrentUser();
        String username = ParseUser.getCurrentUser().getString("name");

        chatMessage.setSenderId(user.getUsername() + SEPARATOR
                + username);

        chatMessage.setBmp(bitmap);
        Utils.saveToFile(chatMessage.getFileName(), chatMessage.getBmp(), this);

        sendPhoto(chatMessage);
        showMessage(chatMessage);
    }

    private void sendPhoto(final ParseChatMessage chatMessage) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        chatMessage.getBmp().compress(Bitmap.CompressFormat.PNG, 60, bos);
        byte[] scaledData = bos.toByteArray();
        ParseFile prsFile = new ParseFile(PHOTO_PNG, scaledData);

        chatMessage.setStatus(STATUS_NOSTATUS);
        final MessageFile prsPhoto = new MessageFile();
        prsPhoto.setPhotoFile(prsFile);
        // Create a media file name

        prsPhoto.setTitle(chatMessage.getFileName());
        prsPhoto.setAuthor(ParseUser.getCurrentUser());

        if (Utils.isNetworkAvailable(NotificationsActivity.this)) {
            prsFile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        chatMessage.setLoadedPercentage(100);
                        adapter.notifyDataSetChanged();
                        prsPhoto.saveInBackground(new SaveCallback() {
                            public void done(ParseException e) {
                                sendPushEfectively(prsPhoto.getObjectId(), chatMessage);
                            }
                        });
                    } else {
                        Toast.makeText(NotificationsActivity.this,
                                NO_SE_PUDO_GUARDAR_LA_IMAGEN_RE_INTENTA,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }, new ProgressCallback() {
                @Override
                public void done(Integer integer) {
                    chatMessage.setLoadedPercentage(integer);
                    adapter.notifyDataSetChanged();
                }
            });
        } else {
            chatMessage.setStatus(STATUS_ERROR);
            adapter.notifyDataSetChanged();
        }

    }

    private void sendPushEfectively(String objectId,
                                    final ParseChatMessage chatMessage) {
        chatMessage.setMessage(BLANK);
        chatMessage.setPhotoId(objectId);
        JSONObject data = null;


            chatMessage.setFacebookId(getFacebookId());


        try {
            data = new JSONObject(chatMessage.toString());
        } catch (JSONException e) {
        }

        ParseUser userFromApp = ParseUser.getCurrentUser();
        long count = 1;
        if (userFromApp.containsKey(MESSAGES_COUNT)) {
            userFromApp.increment(MESSAGES_COUNT);
            count = userFromApp.getInt(MESSAGES_COUNT);
        } else {
            userFromApp.put(MESSAGES_COUNT, 1);
        }

        userFromApp.saveInBackground();

        if (Utils.isNetworkAvailable(NotificationsActivity.this)) {

            // Send push notification to query
            Utils.addToFile(data.toString(), true,
                    OFFICIAL_CHANNEL);
            ParsePush push = new ParsePush();
            //push.setQuery(pq);
            push.setChannel(OFFICIAL_CHANNEL);
            push.setData(data);
            adapter.notifyDataSetChanged();
            push.sendInBackground(new SendCallback() {

                @Override
                public void done(ParseException arg0) {
                    chatMessage.setSend(true);
                    if (arg0 == null) {
                        chatMessage.setStatus(STATUS_SUCCESS);
                        adapter.notifyDataSetChanged();
                    } else {
                        chatMessage.setStatus(STATUS_ERROR);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        } else {
            chatMessage.setStatus(STATUS_ERROR);
            adapter.notifyDataSetChanged();
        }

    }

    public static void showHome(Context activity, String selectedDialog, String number) {
        Intent intent = new Intent(activity, NotificationsActivity.class);
        intent.putExtra(FROM_SELECT_PAGE, true);
        String finalChannel = null;
        intent.putExtra(CHANNEL,  Constants.OFFICIAL_CHANNEL);
        intent.putExtra(NUMERO, number);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }
}
