package com.jmv.fmriacho.chat.notifications;

public class ParseChannel {
	
	private String channelName;
	private boolean notified;
	private String contactPhoto;
	private String role;
	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getContactPhoto() {
		return contactPhoto;
	}

	public void setContactPhoto(String contactPhoto) {
		this.contactPhoto = contactPhoto;
	}

	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public boolean isNotified() {
		return notified;
	}
	public void setNotified(boolean notified) {
		this.notified = notified;
	}

	
}
