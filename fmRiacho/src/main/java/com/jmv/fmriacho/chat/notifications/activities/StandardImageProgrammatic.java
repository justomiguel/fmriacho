package com.jmv.fmriacho.chat.notifications.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jmv.fmriacho.R;
import com.jmv.fmriacho.chat.notifications.zoom.GestureImageView;
import com.rey.material.widget.ProgressView;

import cn.trinea.android.common.service.impl.ImageSDCardCache;
import cn.trinea.android.common.util.CacheManager;

public class StandardImageProgrammatic extends ActionBarActivity {

    public static final ImageSDCardCache IMAGE_CACHE = CacheManager.getImageSDCardCache();


    protected GestureImageView view;

    private static Bitmap bmap;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_standard_image_programmatic);

        this.setTitle(getIntent().getStringExtra("tittle"));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT);
        view = new GestureImageView(this);

        view.setLayoutParams(params);

        ProgressView pv_circular_colors = (ProgressView) findViewById(R.id.progress_pv_circular_colors);
        pv_circular_colors.start();

        if(getIntent().getStringExtra("url")!=null) {
            IMAGE_CACHE.get(getIntent().getStringExtra("url"), view);
        } else {
            view.setImageBitmap(bmap);
        }

        ViewGroup layout = (ViewGroup) findViewById(R.id.layout);

        layout.addView(view);


    }

    public static void showHome(Context context,String tittle, String imageToSHow) {
        Intent intent = new Intent(context, StandardImageProgrammatic.class);
        intent.putExtra("url", imageToSHow);
        intent.putExtra("tittle", tittle);
        context.startActivity(intent);
    }

    public static void showHomeBmp(Activity context, String fileName, Bitmap imageToSHow) {
        Intent intent = new Intent(context, StandardImageProgrammatic.class);
        bmap = imageToSHow;
        intent.putExtra("tittle", fileName);
        context.startActivity(intent);
    }
}
