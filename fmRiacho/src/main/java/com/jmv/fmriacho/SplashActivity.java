package com.jmv.fmriacho;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.jmv.radiosantamarina.MainActivity;

public class SplashActivity extends Activity {

    private final Handler mHandler = new Handler();
    private static final int duration = 5000;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);

    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("first_time", false))
        {

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time",true);
            editor.commit();
            setContentView(R.layout.splashscreen);
            StartAnimations();
            mHandler.postDelayed(mPendingLauncherRunnable, SplashActivity.duration);
        }
        else
        {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            this.startActivity(i);
            this.finish();
        }


    }
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }

    private final Runnable mPendingLauncherRunnable = new Runnable() {

        public void run() {
            final Intent intent = new Intent(SplashActivity.this,
                    MainActivity.class);
            startActivity(intent);
            finish();
        }

    };



}