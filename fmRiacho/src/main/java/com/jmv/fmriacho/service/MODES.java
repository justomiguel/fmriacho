package com.jmv.fmriacho.service;




public enum MODES {

	m_one("CREATED"),
	m_two("CONNECTING"),
	m_three("START_PREPARING"),
	m_four("PREPARED"),
	m_five("STARTED"),
	m_six("PLAYING"),
	m_seven("STOPPED"),
	m_eight("COMPLETED"),
	m_nine("ERROR"),
	m_ten("BUFFERING_START"),
	m_eleven("BUFFERING_END"),
	m_twelve("METADATA_UPDATED"),
	m_thirteen("ALBUM_UPDATED"),
	m_fourteen("DESTROYED");


	private String value;
	
	MODES(String tehvalue) {
		value = tehvalue;
	}

    public String getValue() {
        return value;
    }
}