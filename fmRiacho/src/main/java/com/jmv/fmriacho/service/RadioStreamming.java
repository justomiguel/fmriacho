

package com.jmv.fmriacho.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.RemoteViews;

import com.jmv.fmriacho.BackActivity;
import com.jmv.radiosantamarina.MainActivity;
import com.jmv.fmriacho.R;
import com.jmv.fmriacho.providers.LastFMCover;
import com.parse.ParseInstallation;
import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class RadioStreamming extends Service implements OnErrorListener,
		OnCompletionListener, OnPreparedListener, OnInfoListener {
	final BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			if (action.equals(BROADCAST_PLAYBACK_STOP)){
				if (isRadioPlaying) {
					stop();
					MainActivity.stopchangeicon();
					cambiostop();
				}
			}
			else if (action.equals(BROADCAST_PLAYBACK_PLAY))
			{
				if (isRadioPlaying == false){
					isRadioPlaying = true;
					multiPlayer.playAsync(StationURL());}
			}
			else if (action.equals(BROADCAST_EXIT))
			{
				stop();
				exitNotification();
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		}
	};

	public void cambiostop(){
		if(notifyBuilder!=null && notifyMgr!=null) {
			Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
			remoteViews.setImageViewBitmap(R.id.status_icon, largeIcon);
			remoteViews.setTextViewText(R.id.textView1,getResources().getString(R.string.app_name));
			notifyMgr.notify(NOTIFY_ME_ID,notifyBuilder.build());}

	}


	public static final String

			BROADCAST_PLAYBACK_STOP = "stop",
			BROADCAST_PLAYBACK_PLAY = "pause",
			BROADCAST_EXIT = "exit";
	private static final String SIGNAL_MP3 = "mp3";
	private MediaPlayer mediaPlayer;

	//change to true to use aac decoder
	private static final String nativeplayer = "false";
	public static RemoteViews remoteViews;
	SharedPreferences prefs;
	final Context context = this;
	private String album = "";
	private String artist = "";
	private String track = "";
	private String befCantor = "";
	private String befRola = "";
	private String STATUS_CONNECTING;
	private String STATUS_BUFFERING;
	private String STATUS_READY;
	private String STATUS_PLAYING;
	private String STATUS_STOPPED;
	private String STATUS_ERROR;
	private String STATUS_NETWORK;

	public static final String MODE_ONE = MODES.m_one.getValue();
	public static final String MODE_TWO = MODES.m_two.getValue();
	public static final String MODE_THREE = MODES.m_three.getValue();
	public static final String MODE_FOUR = MODES.m_four.getValue();
	public static final String MODE_FIVE = MODES.m_five.getValue();
	public static final String MODE_SIX = MODES.m_six.getValue();
	public static final String MODE_SEVEN = MODES.m_seven.getValue();
	public static final String MODE_EIGHT = MODES.m_eight.getValue();
	public static final String MODE_NINE = MODES.m_nine.getValue();
	public static final String MODE_TEN = MODES.m_ten.getValue();
	public static final String MODE_ELEVEN = MODES.m_eleven.getValue();
	public static final String MODE_TWELVE = MODES.m_twelve.getValue();
	public static final String MODE_THIRTEEN = MODES.m_thirteen.getValue();
	public static final String MODE_FOURTEEN = MODES.m_fourteen.getValue();


	private String status;
	private boolean isPrepared = false;
	private boolean isPreparingStarted = false;
	private boolean isRadioPlaying = false;
	private Bitmap albumCover = null;
	private int timeCounter = -1;
	private String playingTime;

	private final Handler handler = new Handler();
	private final IBinder binder = new RadioBinder();

	private static final String SIGNAL_AAC = "aac";
	private MultiPlayer multiPlayer;
	private static final int AAC_BUFFER_CAPACITY_MS = 2500;
	private static final int AAC_DECODER_CAPACITY_MS = 700;

	// Notification
	private static final int NOTIFY_ME_ID = 277777;
	private NotificationCompat.Builder notifyBuilder;
	private NotificationManager notifyMgr = null;



	private Timer metadataTimer;
	private Timer playtimeTimer;



	@Override
	public void onCreate() {

		STATUS_CONNECTING = getResources().getString(R.string.st_connecting);
		STATUS_BUFFERING = getResources().getString(R.string.st_buffering);
		STATUS_READY = getResources().getString(R.string.st_ready);
		STATUS_PLAYING = getResources().getString(R.string.st_playing);
		STATUS_STOPPED = getResources().getString(R.string.st_stopped);
		STATUS_ERROR = getResources().getString(R.string.st_error);
		STATUS_NETWORK = getResources().getString(R.string.st_noconnection);
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BROADCAST_PLAYBACK_STOP);
		intentFilter.addAction(BROADCAST_PLAYBACK_PLAY);
		intentFilter.addAction(BROADCAST_EXIT);
		try{
			unregisterReceiver(broadcastReceiver);
		} catch (Exception e){

		}

		registerReceiver(broadcastReceiver, intentFilter);

		try {
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setOnErrorListener(this);
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.setOnInfoListener(this);
			multiPlayer = new MultiPlayer(multiPlayerCallback,
					AAC_BUFFER_CAPACITY_MS, AAC_DECODER_CAPACITY_MS);
		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		sendBroadcast(new Intent(MODE_ONE));

		notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// showNotification();



		startRefreshingMetadata();
		startCounting();
		try {
			java.net.URL.setURLStreamHandlerFactory( new java.net.URLStreamHandlerFactory(){
				public java.net.URLStreamHandler createURLStreamHandler( String protocol ) {

					if ("icy".equals( protocol )) return new com.spoledge.aacdecoder.IcyURLStreamHandler();
					return null;
				}
			});
		}
		catch (Throwable t) {

		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		clearServiceData();

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("first_time", true);
		editor.commit();
		// sendBroadcast(new Intent(MODE_FOURTEEN));
	}

	public void clearServiceData() {
		timeCounter = -1;
		stop();
		resetMetadata();
		isPrepared = false;
		exitNotification();
		stopRefreshingMetadata();
		stopPlayTimer();


	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// showNotification();
		if (isPlaying())
			setStatus(STATUS_PLAYING);
		else if (isPreparingStarted()) {
			setStatus(STATUS_BUFFERING);
			sendBroadcast(new Intent(MODE_THREE));
		} else {
			setStatus("");
			sendBroadcast(new Intent(MODE_FIVE));
		}


		if (mediaPlayer.isPlaying())
			sendBroadcast(new Intent(MODE_SIX));


		return Service.START_NOT_STICKY;
	}

	@Override
	public void onPrepared(MediaPlayer _mediaPlayer) {

		sendBroadcast(new Intent(MODE_FOUR));
		isPrepared = true;
		isPreparingStarted = false;
		timeCounter = 0;
		setStatus("");
		play();
	}

	@Override
	public void onCompletion(MediaPlayer mediaPlayer) {
		isRadioPlaying = false;
		timeCounter = -1;
		mediaPlayer.stop();
		mediaPlayer.reset();
		resetMetadata();
		isPrepared = false;
		setStatus(STATUS_STOPPED);
		sendBroadcast(new Intent(MODE_EIGHT));


	}

	public void prepare() {
		/* ------Station- buffering-------- */
		isPreparingStarted = true;
		setStatus(STATUS_BUFFERING);
		sendBroadcast(new Intent(MODE_THREE));
		showNotification();
		try {
			if (getCurrentStationType().equals(SIGNAL_AAC))
				multiPlayer.playAsync(StationURL());
			else {
				mediaPlayer.setDataSource(StationURL());
				mediaPlayer.prepareAsync();
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			stop();
		} catch (NullPointerException e) {
			e.printStackTrace();
			stop();
		} catch (IllegalStateException e) {
			e.printStackTrace();
			stop();
		} catch (IOException e) {
			e.printStackTrace();
			stop();
		} catch (Exception e) {
			e.printStackTrace();
			stop();
		}

	}





	public boolean isPlaying() {
		if (getCurrentStationType().equals(SIGNAL_AAC))
			return isRadioPlaying;
		else
			return mediaPlayer.isPlaying();
	}

	public boolean isPreparingStarted() {
		return isPreparingStarted;
	}

	public void play() {
		if (isConnected()) {
			if (getCurrentStationType().equals(SIGNAL_AAC)) {
				prepare();
			} else {
				if (isPrepared) {
					isRadioPlaying = true;
					mediaPlayer.start();
					System.out.println("RadioSignal: play");
					setStatus(STATUS_PLAYING);
					sendBroadcast(new Intent(MODE_SIX));
				} else {
					prepare();
				}
			}
		} else
			sendBroadcast(new Intent(MODE_SEVEN));

	}


	public void stop() {

		ParseInstallation.getCurrentInstallation().put("online", false);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		timeCounter = -1;
		resetMetadata();
		isPrepared = false;
		isPreparingStarted = false;
		System.out.println("RadioStation: stop-audio");

		if (getCurrentStationType().equals(SIGNAL_AAC)) {
			if (isRadioPlaying) {
				isRadioPlaying = false;
				multiPlayer.stop();
			}

		} else {
			mediaPlayer.stop();
			mediaPlayer.reset();
			isRadioPlaying = false;

			setStatus("");
			sendBroadcast(new Intent(MODE_SEVEN));
		}

	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {

		if (what == 701) {
			isRadioPlaying = false;
			setStatus(STATUS_BUFFERING);
			sendBroadcast(new Intent(MODE_TEN));
		} else if (what == 702) {
			isRadioPlaying = true;
			setStatus(STATUS_PLAYING);
			sendBroadcast(new Intent(MODE_ELEVEN));
		}

		return false;
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		isRadioPlaying = false;
		timeCounter = -1;

		stop();

		switch (what) {
			case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
				Log.v("ERROR", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK "
						+ extra);
				break;
			case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
				Log.v("ERROR", "MEDIA ERROR SERVER DIED " + extra);
				break;
			case MediaPlayer.MEDIA_ERROR_UNKNOWN:
				Log.v("ERROR", "MEDIA ERROR UNKNOWN " + extra);
				break;
		}

		setStatus(STATUS_ERROR);
		sendBroadcast(new Intent(MODE_NINE));

		return false;
	}



	public String StationName() {

		String nombre = getString(R.string.radioName);
		return nombre;
	}

	public String StationURL() {
		String url = null;
		try {
			url = getString(R.string.streamURL);
		} catch (Exception e) {

			url = getString(R.string.streamURL);
		}
		return url;
	}

	public String getCurrentStationType() {
		String formato = nativeplayer;
		if (formato.equals("false"))
			return SIGNAL_AAC;
		else
			return SIGNAL_MP3;
	}


	public String getTotalStationNumber() {
		String Total ="0";
		return Total;
	}

	public Bitmap getAlbumCover() {
		return albumCover;
	}

	public String getAlbum() {
		return album;
	}

	public String getArtist() {
		return artist;
	}

	public String getTrack() {
		return track;
	}

	public String getPlayingTime() {
		if (timeCounter < 0) {
			playingTime = "";
			return "";
		} else
			return playingTime;
	}

	public void setAlbum(String str) {
		album = str;
	}

	public void setArtist(String str) {
		artist = str;
	}

	public void setTrack(String str) {
		track = str;
	}





	public String getStatus() {
		if (!isConnected())
			this.status = STATUS_NETWORK;


		return this.status;
	}

	public void setStatus(String status) {
		if (!isConnected())
			this.status = STATUS_NETWORK;
		else
			this.status = status;


	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}


	public class RadioBinder extends Binder {
		public RadioStreamming getService() {
			return RadioStreamming.this;
		}
	}
	private PendingIntent makePendingIntent(String broadcast)
	{
		Intent intent = new Intent(broadcast);
		return PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
	}

	public void showNotification() {

		Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome.ttf");

		Bitmap bitlogo= BitmapFactory.decodeResource(context.getResources(),
				R.drawable.ic_launcher);
		remoteViews =new RemoteViews(getPackageName(),
				R.layout.noti_big);
		notifyBuilder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentText("")
				.setContent(remoteViews)
				.setOngoing(true);

		Intent resultIntent = new Intent(this, BackActivity.class);
		resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);

		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		notifyBuilder.setContentIntent(resultPendingIntent);
		//to use custom notification
		remoteViews.setOnClickPendingIntent(R.id.button2, makePendingIntent(BROADCAST_PLAYBACK_STOP));
		remoteViews.setOnClickPendingIntent(R.id.button1, makePendingIntent(BROADCAST_PLAYBACK_PLAY));
		remoteViews.setOnClickPendingIntent(R.id.button3, makePendingIntent(BROADCAST_EXIT));
		remoteViews.setTextViewText(R.id.textView1, getString(R.string.radioName));
		remoteViews.setImageViewBitmap(R.id.status_icon, bitlogo);
		notifyMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifyMgr.notify(NOTIFY_ME_ID, notifyBuilder.build());
	}

	public void clearNotification() {
		if (notifyMgr != null)
			notifyMgr.cancel(NOTIFY_ME_ID);
	}

	public void exitNotification() {
		clearNotification();
		notifyBuilder = null;
		notifyMgr = null;
	}



	public boolean isConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}



	public void updateMetadataTitle(String meta_artist, String meta_track) {

		if (meta_track != null) {
			artist = meta_artist;
			track = meta_track;

			if (artist.equals(befCantor) && track.equals(befRola)) {
			} else {
				befCantor = artist;
				befRola = track;
				clearAlbum();
				sendBroadcast(new Intent(MODE_TWELVE));
				System.out.println(artist);
				System.out.println(track);
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
			}
		} else
			clearAlbum();

	}

	public void startRefreshingMetadata() {

		final Handler handler = new Handler();
		metadataTimer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						try {
							if (getCurrentStationType().equals(SIGNAL_MP3)
									&& (isPlaying() || isPreparingStarted)) {


							}
						} catch (Exception e) {

						}
					}
				});
			}
		};
		metadataTimer.schedule(doAsynchronousTask, 0, 10000);
	}

	public void stopRefreshingMetadata() {
		metadataTimer.cancel();
	}


	public void resetMetadata() {
		artist = "";
		track = "";
		befCantor = "";
		befRola = "";
		clearAlbum();
	}


	public void clearAlbum() {
		album = "";
		albumCover = null;
	}

	public void updateAlbum() {
		try {
			String musicInfo[] = { getArtist(), getTrack() };

			if (!musicInfo[0].equals("") && !musicInfo[1].equals(""))
				new LastFMCoverAsyncTask().execute(musicInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private class LastFMCoverAsyncTask extends
			AsyncTask<String, Integer, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bmp = null;

			try {
				bmp = LastFMCover.getCoverImageFromTrack(getString(R.string.lastFm),
						params[0], params[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap bmp) {
			albumCover = bmp;
			Bitmap bitlogo = null;
			if (bmp != null)

				album = LastFMCover.album;
			String showLastFm= getString(R.string.enableLastFm);
			if (showLastFm.equals("yes")) {
				if (albumCover != null){
					if(notifyBuilder!=null && notifyMgr!=null) {

						remoteViews.setImageViewBitmap(R.id.status_icon, albumCover);
						//notifyBuilder.setLargeIcon(albumCover).setWhen(0);
						notifyMgr.notify(NOTIFY_ME_ID,notifyBuilder.build());}}
				else{
					if(notifyBuilder!=null && notifyMgr!=null) {
						Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
						remoteViews.setImageViewBitmap(R.id.status_icon, largeIcon);
						notifyMgr.notify(NOTIFY_ME_ID,notifyBuilder.build());}
					System.out.println("Cover function not enable on notification");
				}
			}
			else{





				if(notifyBuilder!=null && notifyMgr!=null) {
					Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
					remoteViews.setImageViewBitmap(R.id.status_icon, largeIcon);
					notifyMgr.notify(NOTIFY_ME_ID,notifyBuilder.build());}
				//notifyBuilder.setLargeIcon(albumCover).setWhen(0);
			}

			album = "";
			sendBroadcast(new Intent(MODE_THIRTEEN));
			System.out.println("notification cover updated");
			System.out.println(album);
			System.out.println(albumCover);
		}
	}



	public void startCounting() {
		playtimeTimer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				if (isRadioPlaying == true) {
					timeCounter++;
					int seconds = timeCounter;
					int minutes = seconds / 60;
					int hours = minutes / 60;
					seconds = seconds % 60;
					minutes = minutes % 60;
					if (hours > 0)
						playingTime = String.format("%02d:%02d:%02d", hours,
								minutes, seconds);
					else
						playingTime = String.format("%02d:%02d", minutes,
								seconds);
				}
			}
		};
		playtimeTimer.schedule(doAsynchronousTask, 0, 1000);
	}

	public void stopPlayTimer() {
		playtimeTimer.cancel();
	}


	public final PlayerCallback multiPlayerCallback = new PlayerCallback() {
		@Override
		public void playerStarted() {
			timeCounter = 0;

		}

		@Override
		public void playerPCMFeedBuffer(boolean isPlaying, int bufSizeMs,
										int bufCapacityMs) {

			float percent = bufSizeMs * 100 / bufCapacityMs;
			System.out.println("Buffer = " + percent + "% , " + bufSizeMs
					+ " / " + bufCapacityMs);

			if (isPlaying == true) {
				isPrepared = true;
				isPreparingStarted = false;

				if (bufSizeMs < 500) {
					isRadioPlaying = false;
					setStatus(STATUS_BUFFERING);
					sendBroadcast(new Intent(MODE_TEN));
				} else {
					isRadioPlaying = true;
					setStatus(STATUS_PLAYING);
					sendBroadcast(new Intent(MODE_SIX));
				}
			} else {
				isRadioPlaying = false;
				setStatus(STATUS_BUFFERING);
				sendBroadcast(new Intent(MODE_TEN));
			}

		}

		@Override
		public void playerStopped(int perf) {

			handler.post(new Runnable() {
				@Override
				public void run() {
					timeCounter = -1;
					playingTime = "";

					isRadioPlaying = false;
					isPreparingStarted = false;
					System.out.println("RadioSignal: stop");
					setStatus("");
					sendBroadcast(new Intent(MODE_SEVEN));
				}
			});

		}

		@Override
		public void playerException(Throwable t) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					isRadioPlaying = false;
					isPreparingStarted = false;
					playingTime = "";
					timeCounter = -1;
					System.out.println(": Error Playing Stream");
					setStatus(STATUS_ERROR);
					sendBroadcast(new Intent(MODE_NINE));


				}
			});
		}

		@Override
		public void playerMetadata(String key, String value) {
			System.out.println(key + " ===> " + value);
			System.out.println("MetaInfo======>");


			if ("StreamTitle".equals(key) || "icy-name".equals(key)
					|| "icy-description".equals(key)) {
				final String meta_artist = getArtistFromAAC(value);
				final String meta_track = getTrackFromAAC(value);
				artist = getArtistFromAAC(value);
				track = getTrackFromAAC(value);
				handler.post(new Runnable() {
					@Override
					public void run() {
						if (notifyBuilder != null && notifyMgr != null) {
							remoteViews.setTextViewText(R.id.textView1, artist+" - "+track);
							remoteViews.setImageViewBitmap(R.id.status_icon, albumCover);
							notifyBuilder.setContentText(artist+" - "+track).setWhen(0);
							notifyMgr.notify(NOTIFY_ME_ID, notifyBuilder.build());
						}


						updateMetadataTitle(meta_artist, meta_track);
					}
				});

			}
		}

		@Override
		public void playerAudioTrackCreated(AudioTrack arg0) {
			// TODO Auto-generated method stub

		}
	};

	private String getArtistFromAAC(String streamTitle) {
		int end = streamTitle.indexOf("-");
		if (end <= 0)
			end = streamTitle.indexOf(":");

		String title;
		if (end > 0)
			title = streamTitle.substring(0, end);
		else
			title = streamTitle;
		return title.trim();
	}

	private String getTrackFromAAC(String streamTitle) {
		int start = streamTitle.indexOf("-") + 1;
		if (start <= 0)
			start = streamTitle.indexOf(":") + 1;

		String track;
		if (start > 0)
			track = streamTitle.substring(start);
		else
			track = streamTitle;

		int end = streamTitle.indexOf("(");
		if (end > 0)
			track = streamTitle.substring(start, end);

		end = streamTitle.indexOf("[");
		if (end > 0)
			track = streamTitle.substring(start, end);

		return track.trim();
	}

}
