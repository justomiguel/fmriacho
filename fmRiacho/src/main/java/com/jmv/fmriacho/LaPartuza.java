package com.jmv.fmriacho;

import com.jmv.fmriacho.parse.Incident;
import com.jmv.fmriacho.parse.IncidentType;
import com.jmv.fmriacho.parse.MessageFile;
import com.jmv.fmriacho.utils.Utils;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;

import android.app.Application;
import android.content.Context;

public class LaPartuza extends Application {

	public static Application app = null;

	@Override
	public void onCreate() {
		super.onCreate();
		// Required - Initialize the Parse SDK
		Parse.initialize(this, getString(R.string.parse_app_id),
				getString(R.string.parse_client_key));

		Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);

		ParseObject.registerSubclass(MessageFile.class);
		ParseObject.registerSubclass(Incident.class);
		ParseObject.registerSubclass(IncidentType.class);

		app = this;

		ParseFacebookUtils.initialize(getString(R.string.facebook_app_id));
		//ParsePush.subscribeInBackground(Utils.APP_DOMAIN);
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}

}
