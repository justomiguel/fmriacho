

package com.jmv.fmriacho;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;

import com.facebook.AppEventsLogger;
import com.jmv.fmriacho.service.RadioStreamming;
import com.jmv.fmriacho.dialogs.ConfirmationDialog;
import com.jmv.fmriacho.utils.Utils;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;


public class BaseActivity extends AppCompatActivity {
	@SuppressWarnings("unused")
	private RadioStreamming radioStreamming;
	private Intent bindIntent;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Bind to the service
		bindIntent = new Intent(this, RadioStreamming.class);
		bindService(bindIntent, radioConnection, Context.BIND_AUTO_CREATE);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	private final ServiceConnection radioConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			radioStreamming = ((RadioStreamming.RadioBinder) service).getService();
		}
		@Override
		public void onServiceDisconnected(ComponentName className) {
			radioStreamming = null;
		}
	};

	private ParseUser currentUser;
	protected boolean running = false;
	protected String currentError;

	public static final int DIALOG_SHOW_ERROR = 0;
	public static final int DIALOG_SHOW_HOME = 1;
	public static final int DIALOG_SHOW_ERROR_SYSACAD_NO_ANDA = 2;
	public static final int DIALOG_SHOW_INFO = 3;
	public static final int DIALOG_SHOW_ABOUT = 4;
	public static final int DIALOG_SHOW_DETAILS_EXAM = 5;
	public static final int DIALOG_SHOW_INSCRIBIRSE_EXAM = 6;
	public static final int DIALOG_SHOW_LOG_OUT = 7;
	public static final int DIALOG_SHOW_INSCRIBIRSE_CONFIRM = 8;
	public static final int DIALOG_SHOW_DELETE_CONFIRM = 9;
	public static final int DIALOG_SHOW_DELETE_EXAM = 10;
	public static final int DIALOG_SHOW_DELETE_EXAM_EMPTY = 11;

	public static final int DIALOG_SHOW_ERROR_NO_CHANNEL = 12;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		// Logs 'app deactivate' App Event.
		AppEventsLogger.deactivateApp(this);
	}



	protected void logOut() {
		ParseUser.logOut();
		ParsePush.unsubscribeInBackground(Utils.APP_DOMAIN);
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}

	public String getCurrentError() {
		return currentError;
	}

	public void setCurrentError(String currentError) {
		this.currentError = currentError;
	}
}
