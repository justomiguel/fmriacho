package com.jmv.fmriacho;


import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.jmv.radiosantamarina.MainActivity;


public class BackActivity extends Activity {
	private final Handler mHandler = new Handler();
	private static final int duration = 1;
	 public void onAttachedToWindow() {
			super.onAttachedToWindow();
			Window window = getWindow();
			window.setFormat(PixelFormat.RGBA_8888);
			
		}
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //StartAnimations();
        mHandler.postDelayed(mPendingLauncherRunnable, BackActivity.duration);
    }
    
    @Override
	protected void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mPendingLauncherRunnable);
	}

	private final Runnable mPendingLauncherRunnable = new Runnable() {

		public void run() {
			final Intent intent = new Intent(BackActivity.this,
					MainActivity.class);
			startActivity(intent);
			
			finish();
		}
	};
   
}