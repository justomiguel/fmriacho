package com.jmv.fmriacho;

import com.jmv.fmriacho.utils.Utils;

/**
 * Created by jvargas on 4/30/15.
 */
public class Constants {

    public static final int MESSAGES_TO_SHOW = 20;
    public static final String SEPARATOR = "#";
    public static final String TITTLE = "tittle";
    public static final String CUSTOM_CHANNEL = "customChannel";
    public static final String OFFICIAL_CHANNEL = Utils.APP_DOMAIN;
    public static final String NO_SE_PUDO_GUARDAR_LA_IMAGEN_RE_INTENTA = "Please retry to send the image";
    public static final String MESSAGES_COUNT = "messagesCount";
    public static final String USERCHANNEL = "userchannel";
    public static final String TARGET_SEPARATOR = ".";
    public static final String MAIL_SEPARATOR = "@";
    public static final String FROM_SELECT_PAGE = "fromSelectPage";
    public static final String CHANNEL = "channel";
    public static final String PHOTO_PNG = "photo.png";
    public static final String YYYY_M_MDD_H_HMMSS = "yyyyMMdd_HHmmss";
    public static final String PREFIX = "ING_";
    public static final String COMPLETAR_ACCION_USANDO = "Finish action using";
    public static final String IMAGE_TYPE = "image/*";
    public static final String SELECCIONAR_IMAGEN = "Elegir Imagen";
    public static final String DESDE_MIS_ARCHIVOS = "Desde galeria";
    public static final String DESDE_LA_CAMARA = "Sacar una foto";
    public static final String LAST_CHANNEL_RECEIVED = "lastChannelReceived";
    public static final String CHAT = "Chat FM Riacho";
    public static final String COM_PARSE_PUSH_INTENT_RECEIVE = "com.parse.push.intent.RECEIVE";
    public static final String LA_IMAGEN_SELECCIONA_NO_ES_VALIDA_PARA_SER_ENVIADA = "The image choosen can not being used.";
    public static final  String APP_NAME = "Radio FM Riacho";
    public static final String TURBO_KAT_SAYS = APP_NAME+" dice";
    public static final String FACEBOOK_ID = "facebookId";
    public static final String PHOTO_FILE_NAME = "photoFileName";
    public static final String PHOTO_ID = "photoID";
    public static final String DATE_SENT = "dateSent";
    public static final String ALERT = "alert";
    public static final String COM_PARSE_DATA = "com.parse.Data";
    public static final String SENDER_ID = "senderId";
    public static final String NUMERO = "numero";

    public static final String WORKLOG = "Radio FM Riacho - Worklog";
    public static final String ASSIGNING_USER_TO_THE_ISSUE = "Assigning user...";
    public static final String START = "start";
    public static final String TRUBO_KAT_RESOLUTION = APP_NAME+" - Resolution";

    public static final String IMAGE_URL = "http://www.filecluster.com/howto/wp-content/uploads/2014/07/User-Default.jpg";
    public static final String COLOR_RED_LAN = "#E41A18";
    public static final String COLOR_BLUE_TOP = "#002955";

    public static final String UN_ASSIGNED = "Unassigned";
    public static final String EXTRA_ACCOUNT = "account";
    public static final String EXTRA_FOLDER = "folder";
    public static final String CREATED_AT = "createdAt";
    public static final String WARRIOR = "warrior";
    public static final String CHANGE_ACCOUNT = "Change Account";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String BLANK = "";
    public static final String DEF_VALUE = BLANK;
    public static final String SOMETHING_HAPPENED_S = "Something happened :S ";
    public static final String INBOX = "INBOX";
    public static final String K_9_LANGUAGE = "en";
    public static final String LOG_OUT = "Log Out";
    public static final String ALREADY_LOGGGED_IN = "alreadyLogggedIn";
    public static final String CURRENT_USER = "currentUser";
    public static final String PAGER_DUTY = "PagerDuty";
    public static final String ASSIGNED_TO = "Assigned to ";
    public static final String GRAY_HEXA = "#d9d9d9";
    public static final String REFRESHING_INCIDENTS = "Refreshing incidents...";
    public static final String END = "end";
    public static final String RESOLVED = "Resolved";
    public static final String ASSIGNED = "Assigned";
    public static final String UN_RESOLVED = "Un-resolved";
    public static final String FILTER_ISSUES_BY = "Filter Issues by";
    public static final String FILTER = "Filter";
    public static final String CANCEL = "Cancel";
    public static final String ALL = "All";
    public static final String THUMBNAIL = "thumbnail";
    public static final String NO_CARDS = "No cards";
    public static final String FILTER_BY = "Filter by: ";

    public static final String GET_ROW_COUNT = "getRowCount";
    public static final String NO_ISSUES_FOUND = "No issues found";
    public static final String ZERO = "0";

    public static final String ANSWER = "answer";
    public static final String INCIDENTS = "incidents";

    public static final String CHAT_WITH = "Chat with ";
    public static final String UDPDATE_WORKLOG = "Udpdate worklog";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-mm-dd hh:mm:ss";
    public static final String COMMENT = "comment";
    public static final String SEPARATOR_DASH = " - ";
    public static final String SYSTEM = "SYSTEM";
    public static final String PRIMARY = "PRIMARY";
    public static final String SECONDARY = "SECONDARY";
    public static final String TERTIARY = "TERTIARY";
    public static final String WATCHER = "Watcher";
    public static final String CURRENT_ROLE = "currentRole";
    public static final String ROLE = "role";
}
