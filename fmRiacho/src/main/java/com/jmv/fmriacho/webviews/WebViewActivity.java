package com.jmv.fmriacho.webviews;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.jmv.fmriacho.LaPartuza;
import com.jmv.fmriacho.R;
import com.nineoldandroids.animation.Animator;
import com.rey.material.widget.ProgressView;
import com.rey.material.widget.SnackBar;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemLongClickListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class WebViewActivity extends AppCompatActivity implements OnMenuItemClickListener,
        OnMenuItemLongClickListener {

    private WebView mWebView;

    boolean loadingFinished = true;
    boolean redirect = false;
    private View containerLoading;
    private ProgressView pv_circular_determinate;

    private FragmentManager fragmentManager;
    private DialogFragment mMenuDialogFragment;
    private String url;

    private ProgressView pv_linear_buffer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fragmentManager = getSupportFragmentManager();


       // mMenuDialogFragment = ContextMenuDialogFragment.newInstance((int) getResources().getDimension(R.dimen.tool_bar_height), getMenuObjects());

        url =this.getIntent().getStringExtra("url");

        toolbar.setTitle("Quien Gana");
        toolbar.setSubtitle(url);
        toolbar.setSubtitleTextColor(Color.YELLOW);



        this.containerLoading = findViewById(R.id.containerLoading);

        pv_linear_buffer = (ProgressView)findViewById(R.id.progress_pv_linear_buffer);
        pv_linear_buffer.setProgress(0f);
        pv_linear_buffer.setSecondaryProgress(0f);
        pv_linear_buffer.start();

        pv_circular_determinate = (ProgressView) findViewById(R.id.progress_pv_circular_determinate);
        pv_circular_determinate.setProgress(0f);
        pv_circular_determinate.start();

        mSnackBar = (SnackBar)findViewById(R.id.main_sn);

        mWebView = (WebView) findViewById(R.id.activity_main_webview);
        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // Force links and redirects to open in the WebView instead of in a browser
        //mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                float progressToFloat = Float.parseFloat(String.valueOf(progress)) /100;
                pv_circular_determinate.setProgress(progressToFloat);
                pv_linear_buffer.setProgress(progressToFloat);
                pv_linear_buffer.setSecondaryProgress(progressToFloat*2);
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                if (!loadingFinished) {
                    redirect = true;
                }

                loadingFinished = false;
                view.loadUrl(urlNewString);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                loadingFinished = false;
                //  containerLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(!redirect){
                    loadingFinished = true;
                }

                if(loadingFinished && !redirect){

                    YoYo.with(Techniques.SlideOutRight)
                            .duration(700)
                            .interpolate(new AccelerateDecelerateInterpolator())
                            .withListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    // containerLoading.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    containerLoading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            })
                            .playOn(containerLoading);
                } else{
                    redirect = false;
                }

            }
        });



        mWebView.loadUrl(url);

    }

    private List<MenuObject> getMenuObjects() {
        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.ic_launcher);

        MenuObject send = new MenuObject("Compartir pagina con un amigo");
        send.setResource(R.drawable.ic_launcher);

        MenuObject addFav = new MenuObject("Compartir pagina en Facebook");
        addFav.setResource(R.drawable.ic_launcher);

        MenuObject twitter = new MenuObject("Compartir pagina en Twitter");
        twitter.setResource(R.drawable.ic_launcher);

        menuObjects.add(close);
        menuObjects.add(send);
        menuObjects.add(addFav);
        menuObjects.add(twitter);

        for(MenuObject object:menuObjects){
            object.setBgDrawable(getResources().getDrawable(R.drawable.ps__button_dropbox));
            object.setDividerColor(Color.parseColor("#00000000"));
        }
        return menuObjects;
    }


    private SnackBar mSnackBar;

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch (position) {
            case 0:
                break;
            case 1:
                String shareBody = url;
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Llegue a esta pagina usando la app Quien Gana! Echale un vistazo!");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Compartir por"));
                break;
            case 2:

                break;
            default:
                // Create intent using ACTION_VIEW and a normal Twitter url:
                String tweetUrl =
                        String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                                urlEncode("Llegue a esta pagina gracias #quiengana2015"), urlEncode(url));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

                // Narrow down to official Twitter app, if available:
                List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                        intent.setPackage(info.activityInfo.packageName);
                    }
                }

                startActivity(intent);
                break;
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            // throw new RuntimeException("URLEncoder.encode() failed for " + s);
            Toast.makeText(LaPartuza.app, "Unable to post to twitter", Toast.LENGTH_SHORT).show();
        }
        return "";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onMenuItemLongClick(View view, int i) {

    }
}
