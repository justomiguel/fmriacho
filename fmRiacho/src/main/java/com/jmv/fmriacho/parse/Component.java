package com.jmv.fmriacho.parse;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by jvargas on 5/1/15.
 */
@ParseClassName("Component")
public class Component extends ParseObject {

    public String getTittle() {
        return getString("tittle");
    }

    public void setTittle(String tittle) {
        this.put("tittle",tittle);
    }
    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        this.put("description",description);
    }


}
