package com.jmv.fmriacho.parse;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;

/**
 * Created by jvargas on 4/30/15.
 */
@ParseClassName("Incident")
public class Incident extends ParseObject {

    public ParseUser getWarrior() {
        return getParseUser("warrior");
    }

    public void setWarrior(ParseUser warrior) {
        this.put("warrior", warrior);
    }

    public ParseFile getFile() {
        return getParseFile("file");
    }

    public void setFile(ParseFile file) {
        this.put("file", file);
    }

    public String getTittle() {
        return getString("tittle");
    }

    public void setTittle(String tittle) {
        this.put("tittle",tittle);
    }

    public IncidentType getType() {
        return (IncidentType) get("type");
    }

    public void setType(IncidentType type) {
        this.put("type",type);
    }

    public Priority getPriority() {
        return (Priority) get("priority");
    }

    public void setPriority(Priority priority) {
        this.put("priority",priority);
    }

    public Component getComponent() {
        return (Component) get("component");
    }

    public void setComponent(Component component) {
        this.put("component",component);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        this.put("description",description);
    }

    public String getComment() {
        return getString("comment");
    }

    public void setComment(String comment) {
        this.put("comment",comment);
    }

    public Date getStart() {
        return getDate("start");
    }

    public void setStart(Date start) {
        this.put("start", start);
    }

    public Date getEnd() {
        return getDate("end");
    }

    public void setEnd(Date end) {
        this.put("end", end);
    }

    public String getSource() {
        return getString("source");
    }

    public void setSource(String source) {
        this.put("source", source);
    }

    public long getSenddate() {
        return getLong("senddate");
    }

    public void setSenddate(long senddate) {
        this.put("senddate", senddate);
    }

    public String getResolution() {
        return getString("resolution");
    }

    public void setResolution(String resolution) {
        this.put("resolution",resolution);
    }

    public long getDuration() {
        return getLong("duration");
    }

    public void setDuration(long duration) {
        this.put("duration",duration);
    }

    public Date getCreatedAt() {
        return getDate("createdAt");
    }

    public Date getUpdatedAt() {
        return getDate("updatedAt");
    }


}
