

package com.jmv.radiosantamarina;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings.System;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.jmv.fmriacho.BaseActivity;
import com.jmv.fmriacho.Constants;
import com.jmv.fmriacho.R;
import com.jmv.fmriacho.chat.notifications.activities.NotificationsActivity;
import com.jmv.fmriacho.utils.Utils;
import com.jmv.fmriacho.providers.LastFMCover;
import com.jmv.fmriacho.service.RadioStreamming;
import com.jmv.fmriacho.webviews.AboutActivity;
import com.jmv.fmriacho.webviews.FbActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


@SuppressLint("NewApi")
public class MainActivity extends BaseActivity {


	@SuppressWarnings("unused")
	private static boolean audiOff;
	final Context context = this;
	private static Button BtnPlay;
	private static Button BtnStop;

	//all textviews
	private static TextView radioStatus;
	private static TextView radioCounter;
	private static TextView artistName;
	private static TextView trackName;


	private Intent bindIntent;

	private static final int LOGIN_REQUEST = 65525;


	private String loggedInUserName;
	private String loggedInUserPassword;

	private UpRe UpRe;
	public static RadioStreamming radioStreamming;
	private AdView adView;
	@SuppressWarnings("unused")
	private String LOADING;
	private static final String SIGNAL_AAC = "aac";
	@SuppressWarnings("unused")
	private Handler handler;
	//during call audio
	private boolean audioOncall = false;
	private TelephonyManager phoneMngr;
	//volume
	private AudioManager AudioMgr;
	private SeekBar volControl;
	private ContentObserver mVolumeObserver;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.playerfinal);

		Button share = (Button) findViewById(R.id.btnFacebook);

		share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				share();
			}
		});

		Button off = (Button) findViewById(R.id.btnOff);

		off.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Off();
			}
		});

		Button rate = (Button) findViewById(R.id.rate);

		rate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rate();
			}
		});


		try {
			bindIntent = new Intent(this, RadioStreamming.class);
			bindService(bindIntent, radioConnection, BIND_AUTO_CREATE);
		} catch (Exception e) {
			e.printStackTrace();
		}


		phoneMngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if (phoneMngr != null) {
			phoneMngr.listen(phoneStateListener,
					PhoneStateListener.LISTEN_CALL_STATE);
		}

		handler = new Handler();
		MusicOn();

		ParseInstallation.getCurrentInstallation().put("online", true);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		getOnLineStatus();
	}


	public void getOnLineStatus() {
		final TextView online = (TextView) findViewById(R.id.online);
		final TextView total = (TextView) findViewById(R.id.oyentes);

		final HashMap<String, Object> params = new HashMap<String, Object>();
		ParseCloud.callFunctionInBackground("getInstallations", params, new FunctionCallback<Integer>() {
			public void done(final Integer value, ParseException e) {
				if (e == null) {
					try {
						int totalNUmber = value;
						String textToPLace = getString(R.string.total_users) + " " + totalNUmber;
						total.setText(textToPLace);
					} catch (Exception e2) {

					}
				}
			}
		});

		final HashMap<String, Object> params2 = new HashMap<String, Object>();

		ParseCloud.callFunctionInBackground("getOnLine", params2, new FunctionCallback<Integer>() {
			public void done(final Integer value, ParseException e) {
				if (e == null) {
					try {
						int totalNUmber = value;
						String textToPLace = getString(R.string.total_online) + " " + totalNUmber;
						online.setText(textToPLace);
					} catch (Exception e2) {

					}
				}
			}
		});

	}


	public boolean isLoggedIn() {
		return ParseUser.getCurrentUser() != null;
	}

	public void logIn() {
		ParseLoginBuilder builder = new ParseLoginBuilder(this);
		Intent parseLoginIntent = builder
				.setParseLoginEnabled(false)
				.setParseLoginButtonText("Entrar")
				.setParseSignupButtonText("Registrarse")
				.setParseLoginHelpText("Te olvidaste de tu password?")
				.setParseLoginInvalidCredentialsToastText(
						"El mail no es correcto")
				.setParseLoginEmailAsUsername(true)
				.setParseSignupSubmitButtonText("Enviar registracion")
				.setFacebookLoginEnabled(true)
				.setFacebookLoginButtonText("Ingresar con Facebook")
				.setTwitterLoginEnabled(false)
				.setTwitterLoginButtontext("Twitter").build();
		startActivityForResult(parseLoginIntent, LOGIN_REQUEST);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == LOGIN_REQUEST){
			try{
				ParsePush.subscribeInBackground(Utils.APP_DOMAIN);
				ParseInstallation.getCurrentInstallation().saveInBackground();
				ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
				ParseUser user = ParseUser.getCurrentUser();
				if (user != null && ParseFacebookUtils.isLinked(user)){
					makeMeRequest();
				}
			} catch (Exception e){
				ParseUser user = ParseUser.getCurrentUser();
				if (user !=null){
					ParseUser.logOut();
					ParsePush.unsubscribeInBackground(Utils.APP_DOMAIN);
					ParseInstallation.getCurrentInstallation().saveInBackground();
				}
				Toast.makeText(this, "Se produjo un error al intentar acceder, intenta de nuevo", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void makeMeRequest() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseInstallation.getCurrentInstallation().put("user", currentUser);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		if (!currentUser.has("profile")) {
			Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
					new Request.GraphUserCallback() {
						@Override
						public void onCompleted(GraphUser user, Response response) {
							if (user != null ) {
								// Create a JSON object to hold the profile info
								JSONObject userProfile = new JSONObject();
								try {
									// Populate the JSON object
									userProfile.put("facebookId", user.getId());
									userProfile.put("name", user.getName());
									if (user.getProperty("gender") != null) {
										userProfile.put("gender",
												user.getProperty("gender"));
									}
									if (user.getProperty("email") != null) {
										userProfile.put("email",
												user.getProperty("email"));
									}
									// Save the user profile info in a user property
									ParseUser currentUser = ParseUser
											.getCurrentUser();
									currentUser.put("profile", userProfile);
									currentUser.saveInBackground();
									// Show the user info
									NotificationsActivity.showHome(MainActivity.this, Constants.OFFICIAL_CHANNEL, null);
								} catch (JSONException e) {
									NotificationsActivity.showHome(MainActivity.this, Constants.OFFICIAL_CHANNEL, null);
								}
							} else if (response.getError() != null) {
								NotificationsActivity.showHome(MainActivity.this, Constants.OFFICIAL_CHANNEL, null);
							}
							//showProgress(false);
						}
					});
			request.executeAsync();
		} else {
			NotificationsActivity.showHome(MainActivity.this, Constants.OFFICIAL_CHANNEL, null);
		}

	}

	public void MusicOn() {
		try {

			Typeface font2 = Typeface.createFromAsset(getAssets(),
					"main.ttf");


			String SizeMain = getResources().getString(R.string.radioname_text_size);
			final int sizemain = Integer.parseInt(SizeMain);
			String SizeStatus = getResources().getString(R.string.radiostatus_text_size);
			final int sizestatus = Integer.parseInt(SizeStatus);
			String SizeCounter = getResources().getString(R.string.radiocounter_text_size);
			final int sizecounter = Integer.parseInt(SizeCounter);
			String SizeTrack = getResources().getString(R.string.trackname_text_size);
			final int sizetrack = Integer.parseInt(SizeTrack);
			String SizeArtist = getResources().getString(R.string.artistname_text_size);
			final int sizeartist = Integer.parseInt(SizeArtist);

			BtnPlay = (Button) this.findViewById(R.id.BtnPlay);
			BtnStop = (Button) this.findViewById(R.id.BtnStop);

			BtnPlay.setEnabled(true);
			BtnStop.setEnabled(false);


			radioCounter = (TextView) this.findViewById(R.id.radioCounter);
			radioCounter.setTextColor(Color.parseColor("#FFFFFF"));
			radioCounter.setTypeface(font2);
			radioCounter.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizecounter);
			radioStatus = (TextView) this.findViewById(R.id.radioStatus);
			radioStatus.setTextColor(Color.parseColor("#FFFFFF"));
			radioStatus.setTypeface(font2);
			radioStatus.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizestatus);
			artistName = (TextView) this.findViewById(R.id.artistName);
			artistName.setTextColor(Color.parseColor("#FFFFFF"));
			artistName.setTypeface(font2);
			if (getString(R.string.showArtist).equals("no")) {
				artistName.setVisibility(View.GONE);}
			artistName.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeartist);
			trackName = (TextView) this.findViewById(R.id.trackName);
			trackName.setTextColor(Color.parseColor("#FFFFFF"));
			trackName.setTypeface(font2);
			trackName.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizetrack);
			if (getString(R.string.showTrack).equals("no")) {
				trackName.setVisibility(View.GONE);}
			LOADING = getResources().getString(R.string.st_buffering);
			AudioMgr = (AudioManager) getSystemService(AUDIO_SERVICE);

			int maxVolume = AudioMgr
					.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			int curVolume = AudioMgr.getStreamVolume(AudioManager.STREAM_MUSIC);
			volControl = (SeekBar) findViewById(R.id.volumebar);
			volControl.setMax(maxVolume);
			volControl.setProgress(curVolume);
			volControl
					.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


						@Override
						public void onProgressChanged(SeekBar a0, int a1,
													  boolean a2) {
							AudioMgr.setStreamVolume(AudioManager.STREAM_MUSIC,
									a1, 0);
						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {
							// TODO Auto-generated method stub

						}
					});

			Handler mHandler = new Handler();
			// in onCreate put
			mVolumeObserver = new ContentObserver(mHandler) {
				@Override
				public void onChange(boolean selfChange) {
					super.onChange(selfChange);
					if (volControl != null && AudioMgr != null) {
						int volume = AudioMgr
								.getStreamVolume(AudioManager.STREAM_MUSIC);
						volControl.setProgress(volume);
					}
				}

			};
			this.getContentResolver()
					.registerContentObserver(
							System.getUriFor(System.VOLUME_SETTINGS[AudioManager.STREAM_MUSIC]),
							false, mVolumeObserver);

			// vloume
			startService(new Intent(this, RadioStreamming.class));

			displayAd();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		volControl = (SeekBar) findViewById(R.id.volumebar);
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			int index = volControl.getProgress();
			volControl.setProgress(index + 1);
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			int index = volControl.getProgress();
			volControl.setProgress(index - 1);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void displayAd() {
		String showAd= getString(R.string.enableads);
		if (showAd.equals("yes")) {
			// Create the adView
			try {

				if (adView != null) {
					adView.destroy();
				}

				adView = new AdView(this);
				adView.setAdSize(AdSize.SMART_BANNER);
				adView.setAdUnitId(getString(R.string.publisherID));

				// Add the AdView to the view hierarchy. The view will have no size
				// until the ad is loaded.
				LinearLayout layout = (LinearLayout) findViewById(R.id.adLayout);
				layout.addView(adView);

				// Create an ad request. Check logcat output for the hashed device ID to
				// get test ads on a physical device.
				AdRequest adRequest = new AdRequest.Builder()
						.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
						.addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
						.build();

				// Start loading the ad in the background.
				adView.loadAd(adRequest);
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			LinearLayout layout = (LinearLayout) findViewById(R.id.adLayout);
			layout.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
			layout.setVisibility(View.INVISIBLE);
		}
	}

	public void updatePlayTimer() {
		radioCounter.setText(radioStreamming.getPlayingTime());

		final Handler handler = new Handler();
		Timer timer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						radioCounter.setText(radioStreamming.getPlayingTime());
					}
				});
			}
		};
		timer.schedule(doAsynchronousTask, 0, 1000);
	}

	public void StartMusic(View view) {
		radioStreamming.play();
	}

	public void startOncall (){
		radioStreamming.play();


	}
	public static void stopchangeicon(){
		BtnPlay.setEnabled(true);
		BtnStop.setEnabled(false);
		BtnStop.setVisibility(View.INVISIBLE);
		BtnPlay.setVisibility(View.VISIBLE);
		artistName.setText("");
		trackName.setText("");
		radioCounter.setText("00:00");
		radioStatus.setText("");

	}


	public void StopMusic(View view) {
		radioStreamming.stop();
		resetMetadata();
		UpdatetoLogo();
		radioStreamming.exitNotification();
	}

	public void stopOncall(){
		radioStreamming.stop();

	}







	@SuppressWarnings("deprecation")
	public void UpdatetoLogo() {
		try {

			LinearLayout linear = (LinearLayout) findViewById (R.id.MainPlayer);
			linear.setBackgroundResource(R.drawable.background);

		} catch(Exception e) {

		}
	}

	@SuppressWarnings("deprecation")
	public void updateAlbum() {
		LinearLayout linear = (LinearLayout) findViewById (R.id.MainPlayer);

		String artist = radioStreamming.getArtist();
		String track = radioStreamming.getTrack();
		Bitmap albumCover = radioStreamming.getAlbumCover();



		if (albumCover == null || (artist.equals("") && track.equals("")))
			UpdatetoLogo();
		else {
			int targetWidth = 800;
			int targetHeight = 800;
			Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
			BitmapShader shader;
			shader = new BitmapShader(albumCover, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

			Paint paint = new Paint();
			paint.setAntiAlias(true);
			paint.setShader(shader);
			Canvas canvas = new Canvas(targetBitmap);
			Path path = new Path();
			path.addCircle(((float) targetWidth - 1) / 2,
					((float) targetHeight - 1) / 2,
					(Math.min(((float) targetWidth), ((float) targetHeight)) / 2), Path.Direction.CCW);
			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			paint.setStyle(Paint.Style.STROKE);
			canvas.clipPath(path);
			Bitmap sourceBitmap = albumCover;
			canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),sourceBitmap.getHeight()),
					new Rect(0, 0, targetWidth,targetHeight), null);



			BitmapDrawable d = new BitmapDrawable(albumCover);

			linear.setBackgroundDrawable(d);
			// uncomment the line bellow to show cover round
			//stationImageView.setImageBitmap(targetBitmap);
			radioStreamming.setAlbum(LastFMCover.album);

			if (radioStreamming.getAlbum().length()
					+ radioStreamming.getArtist().length() > 50) {

			}
		}
	}





	public void updateMetadata() {
		String artist = radioStreamming.getArtist();
		artistName.setText(artist);

		String track = radioStreamming.getTrack();
		trackName.setText(track);

	}

	public void resetMetadata() {
		String status = radioStreamming.getStatus();
		radioStreamming.resetMetadata();
		artistName.setText("");
		trackName.setText("");
		radioCounter.setText("00:00");
		radioStatus.setText(status);
	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("first_time", false);
		editor.commit();

		ParseInstallation.getCurrentInstallation().put("online", false);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		if (radioStreamming != null) {
			if (!radioStreamming.isPlaying() && !radioStreamming.isPreparingStarted()) {
				radioStreamming.stopService(bindIntent);
				ParseInstallation.getCurrentInstallation().put("online", false);
				ParseInstallation.getCurrentInstallation().saveInBackground();

			}
		}

		if (adView != null) {
			adView.destroy();
		}

		if (phoneMngr != null) {
			phoneMngr.listen(phoneStateListener,
					PhoneStateListener.LISTEN_NONE);
		}

		unbindDrawables(findViewById(R.id.MainPlayer));
		Runtime.getRuntime().gc();
	}

	private void unbindDrawables(View view) {
		try {
			if (view.getBackground() != null) {
				view.getBackground().setCallback(null);
			}
			if (view instanceof ViewGroup) {
				for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
					unbindDrawables(((ViewGroup) view).getChildAt(i));
				}
				((ViewGroup) view).removeAllViews();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (UpRe != null)
			unregisterReceiver(UpRe);
		// finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (UpRe == null)
			UpRe = new UpRe();
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_ONE));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_FOURTEEN));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_FIVE));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_TWO));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_THREE));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_FOUR));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_SIX));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_SEVEN));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_EIGHT));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_NINE));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_TEN));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_ELEVEN));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_TWELVE));
		registerReceiver(UpRe, new IntentFilter(RadioStreamming.MODE_THIRTEEN));

		if (audioOncall) {
			radioStreamming.play();
			audioOncall = false;
		}

		if (radioStreamming != null) {

		}
	}





	private class UpRe extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(RadioStreamming.MODE_ONE)) {

			} else if (intent.getAction().equals(RadioStreamming.MODE_FOURTEEN)) {
				BtnPlay.setEnabled(true);

				BtnStop.setEnabled(false);
				BtnStop.setVisibility(View.INVISIBLE);
				BtnPlay.setVisibility(View.VISIBLE);

				UpdatetoLogo();
				updateMetadata();
				updateStatus();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();
				}else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(RadioStreamming.MODE_FIVE)) {

				BtnPlay.setVisibility(View.VISIBLE);


				BtnPlay.setEnabled(true);
				BtnStop.setEnabled(false);
				BtnStop.setVisibility(View.INVISIBLE);
				updateStatus();
				updateMetadata();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(RadioStreamming.MODE_TWO)) {

				BtnPlay.setVisibility(View.INVISIBLE);

				BtnPlay.setEnabled(false);
				BtnStop.setEnabled(true);
				BtnStop.setVisibility(View.VISIBLE);
				updateStatus();
				updateMetadata();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(
					RadioStreamming.MODE_THREE)) {

				BtnPlay.setVisibility(View.INVISIBLE);

				BtnPlay.setEnabled(false);
				BtnStop.setEnabled(true);
				BtnStop.setVisibility(View.VISIBLE);
				updateStatus();
				updateMetadata();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(RadioStreamming.MODE_FOUR)) {
				BtnPlay.setEnabled(true);
				BtnStop.setVisibility(View.INVISIBLE);
				BtnStop.setEnabled(false);
				BtnPlay.setVisibility(View.VISIBLE);
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
				updateStatus();
				updateMetadata();
			} else if (intent.getAction().equals(
					RadioStreamming.MODE_TEN)) {
				updateStatus();
			} else if (intent.getAction().equals(
					RadioStreamming.MODE_ELEVEN)) {
				updateStatus();
			} else if (intent.getAction().equals(RadioStreamming.MODE_SIX)) {
				if (radioStreamming.getCurrentStationType().equals(SIGNAL_AAC)) {
					BtnPlay.setEnabled(false);
					BtnPlay.setVisibility(View.INVISIBLE);
					BtnStop.setEnabled(true);
					BtnStop.setVisibility(View.VISIBLE);
					updateMetadata();
					String showLastFm= getString(R.string.enableLastFm);
					if (showLastFm.equals("yes")) {
						updateAlbum();}
					else{
						java.lang.System.out.println("LastFm is not enable");
					}
				} else {
					BtnPlay.setEnabled(false);
					BtnStop.setEnabled(true);
					BtnStop.setVisibility(View.VISIBLE);
					BtnPlay.setVisibility(View.INVISIBLE);

					updateMetadata();
					String showLastFm= getString(R.string.enableLastFm);
					if (showLastFm.equals("yes")) {
						updateAlbum();}
					else{
						java.lang.System.out.println("LastFm is not enable");
					}
				}

				updateStatus();
			}else if (intent.getAction().equals(RadioStreamming.MODE_SEVEN)) {
				BtnPlay.setEnabled(true);
				BtnPlay.setVisibility(View.VISIBLE);
				BtnStop.setEnabled(false);
				BtnStop.setVisibility(View.INVISIBLE);
				updateStatus();
				updateMetadata();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(RadioStreamming.MODE_EIGHT)) {
				BtnPlay.setEnabled(true);
				BtnStop.setVisibility(View.INVISIBLE);
				BtnStop.setEnabled(false);
				BtnPlay.setVisibility(View.VISIBLE);
				updateStatus();
				updateMetadata();
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}
			} else if (intent.getAction().equals(RadioStreamming.MODE_NINE)) {
				BtnPlay.setEnabled(true);
				BtnStop.setVisibility(View.INVISIBLE);
				BtnStop.setEnabled(false);
				BtnPlay.setVisibility(View.VISIBLE);

				updateStatus();
				updateMetadata();
			} else if (intent.getAction().equals(
					RadioStreamming.MODE_TWELVE)) {
				updateMetadata();
				updateStatus();
				UpdatetoLogo();
			} else if (intent.getAction().equals(
					RadioStreamming.MODE_THIRTEEN)) {
				String showLastFm= getString(R.string.enableLastFm);
				if (showLastFm.equals("yes")) {
					updateAlbum();}
				else{
					java.lang.System.out.println("LastFm is not enable");
				}

			}
		}
	}



	public void updateStatus() {
		try {
			String status = radioStreamming.getStatus();
			String title = radioStreamming.StationName();
			radioStatus.setText(status);
		} catch (Exception e) {

		}
	}


	private final ServiceConnection radioConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			radioStreamming = ((RadioStreamming.RadioBinder) service).getService();
			updateStatus();
			updateMetadata();
			String showLastFm= getString(R.string.enableLastFm);
			if (showLastFm.equals("yes")) {
				updateAlbum();}
			else{
				java.lang.System.out.println("LastFm is not enable");
			}
			updatePlayTimer();
			radioStreamming.showNotification();
			String AutoPlay= getString(R.string.enableAutoplay);
			if (AutoPlay.equals("yes")) {
				radioStreamming.play();}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			radioStreamming = null;
		}
	};


	PhoneStateListener headphones = new PhoneStateListener() {

		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)){
				String mes;
				int state = intent.getIntExtra("state", 4);
				if(state == 0){
					mes ="out";
				}else if(state == 1){
					mes ="in";
				}else {
					mes ="others";
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Headset broadcast");
				builder.setMessage(mes);
				builder.setPositiveButton("Okey-dokey", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.create().show();

			}

		}
	};

	PhoneStateListener phoneStateListener = new PhoneStateListener() {

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state){

				case TelephonyManager.CALL_STATE_IDLE:
					//CALL_STATE_IDLE;
					if (audioOncall) {
						resetMetadata();
						UpdatetoLogo();
						Intent restart = new Intent(getApplicationContext(), MainActivity.class);

						startActivity(restart);
					}
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					//CALL_STATE_OFFHOOK;
					if (audioOncall) {
						resetMetadata();
						UpdatetoLogo();
						Intent restart = new Intent(getApplicationContext(), MainActivity.class);

						startActivity(restart);
					}
					break;
				case TelephonyManager.CALL_STATE_RINGING:
					//CALL_STATE_RINGING
					audioOncall = radioStreamming.isPlaying();
					stopOncall();
					break;
				default:
					break;


			}
			super.onCallStateChanged(state, incomingNumber);
		}
	};



	//Buttons Functions
	public void btnShare(View v) {

		String appname= getResources().getString(R.string.app_name);
		Intent in = getIntent();
		String radioname = in.getStringExtra("");//poner aqui la radio station
		String packurl = getString(R.string.goplay);


		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");


		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,"I'm listening "+ radioname + " on "+ appname+" " + packurl);
		startActivity(Intent.createChooser(sharingIntent, "Share via"));
	}

	public void Off(){
		audiOff = true;
		AlertDialog.Builder ad = new AlertDialog.Builder(this);
		ad.setTitle(getString(R.string.radioName));
		ad.setMessage(getResources().getString(R.string.exit_msj));
		ad.setCancelable(true);
		ad.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (radioStreamming != null) {
							radioStreamming.exitNotification();
							radioStreamming.stop();
							audiOff = true;
							logOut();
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());

						} else {
							Intent setIntent = new Intent(Intent.ACTION_MAIN);
							setIntent.addCategory(Intent.CATEGORY_HOME);
							setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(setIntent);

						}
					}
				});

		ad.setNegativeButton("No", null);
		ad.show();


	}

	public void rate(){
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.goplay)));
		startActivity(intent);
	}

	public void share(){

		String urlToShare = getString(R.string.goplay);
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT,getResources().getString(R.string.share_msj));
		intent.putExtra(Intent.EXTRA_TEXT, urlToShare);
		startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_via)));
	}

	public void btnTwitter(View v) {
		Intent twitter = new Intent(getApplicationContext(), AboutActivity.class);
		startActivity(twitter);
	}

	public void btnAbout(View v) {
		ParseUser user = ParseUser.getCurrentUser();
		if (user != null && ParseFacebookUtils.isLinked(user)){
			makeMeRequest();
		} else {
			logIn();
		}
	}
	public void btnFacebook(View v) {
		Intent facebook = new Intent(getApplicationContext(), FbActivity.class);
		startActivity(facebook);
	}



}