/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.android.gms.location;

public final class R {
	public static final class attr {
		public static final int adSize = 0x7f010035;
		public static final int adSizes = 0x7f010036;
		public static final int adUnitId = 0x7f010037;
		public static final int appTheme = 0x7f010221;
		public static final int buyButtonAppearance = 0x7f010228;
		public static final int buyButtonHeight = 0x7f010225;
		public static final int buyButtonText = 0x7f010227;
		public static final int buyButtonWidth = 0x7f010226;
		public static final int cameraBearing = 0x7f0100dc;
		public static final int cameraTargetLat = 0x7f0100dd;
		public static final int cameraTargetLng = 0x7f0100de;
		public static final int cameraTilt = 0x7f0100df;
		public static final int cameraZoom = 0x7f0100e0;
		public static final int circleCrop = 0x7f0100da;
		public static final int environment = 0x7f010222;
		public static final int fragmentMode = 0x7f010224;
		public static final int fragmentStyle = 0x7f010223;
		public static final int imageAspectRatio = 0x7f0100d9;
		public static final int imageAspectRatioAdjust = 0x7f0100d8;
		public static final int liteMode = 0x7f0100e1;
		public static final int mapType = 0x7f0100db;
		public static final int maskedWalletDetailsBackground = 0x7f01022b;
		public static final int maskedWalletDetailsButtonBackground = 0x7f01022d;
		public static final int maskedWalletDetailsButtonTextAppearance = 0x7f01022c;
		public static final int maskedWalletDetailsHeaderTextAppearance = 0x7f01022a;
		public static final int maskedWalletDetailsLogoImageType = 0x7f01022f;
		public static final int maskedWalletDetailsLogoTextColor = 0x7f01022e;
		public static final int maskedWalletDetailsTextAppearance = 0x7f010229;
		public static final int uiCompass = 0x7f0100e2;
		public static final int uiMapToolbar = 0x7f0100ea;
		public static final int uiRotateGestures = 0x7f0100e3;
		public static final int uiScrollGestures = 0x7f0100e4;
		public static final int uiTiltGestures = 0x7f0100e5;
		public static final int uiZoomControls = 0x7f0100e6;
		public static final int uiZoomGestures = 0x7f0100e7;
		public static final int useViewLifecycle = 0x7f0100e8;
		public static final int windowTransitionStyle = 0x7f010063;
		public static final int zOrderOnTop = 0x7f0100e9;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f0d0035;
		public static final int common_signin_btn_dark_text_default = 0x7f0d0036;
		public static final int common_signin_btn_dark_text_disabled = 0x7f0d0037;
		public static final int common_signin_btn_dark_text_focused = 0x7f0d0038;
		public static final int common_signin_btn_dark_text_pressed = 0x7f0d0039;
		public static final int common_signin_btn_default_background = 0x7f0d003a;
		public static final int common_signin_btn_light_text_default = 0x7f0d003b;
		public static final int common_signin_btn_light_text_disabled = 0x7f0d003c;
		public static final int common_signin_btn_light_text_focused = 0x7f0d003d;
		public static final int common_signin_btn_light_text_pressed = 0x7f0d003e;
		public static final int common_signin_btn_text_dark = 0x7f0d0081;
		public static final int common_signin_btn_text_light = 0x7f0d0082;
		public static final int wallet_bright_foreground_disabled_holo_light = 0x7f0d0067;
		public static final int wallet_bright_foreground_holo_dark = 0x7f0d0068;
		public static final int wallet_bright_foreground_holo_light = 0x7f0d0069;
		public static final int wallet_dim_foreground_disabled_holo_dark = 0x7f0d006a;
		public static final int wallet_dim_foreground_holo_dark = 0x7f0d006b;
		public static final int wallet_dim_foreground_inverse_disabled_holo_dark = 0x7f0d006c;
		public static final int wallet_dim_foreground_inverse_holo_dark = 0x7f0d006d;
		public static final int wallet_highlighted_text_holo_dark = 0x7f0d006e;
		public static final int wallet_highlighted_text_holo_light = 0x7f0d006f;
		public static final int wallet_hint_foreground_holo_dark = 0x7f0d0070;
		public static final int wallet_hint_foreground_holo_light = 0x7f0d0071;
		public static final int wallet_holo_blue_light = 0x7f0d0072;
		public static final int wallet_link_text_light = 0x7f0d0073;
		public static final int wallet_primary_text_holo_light = 0x7f0d0085;
		public static final int wallet_secondary_text_holo_dark = 0x7f0d0086;
	}
	public static final class drawable {
		public static final int common_full_open_on_phone = 0x7f020093;
		public static final int common_ic_googleplayservices = 0x7f020094;
		public static final int common_signin_btn_icon_dark = 0x7f020095;
		public static final int common_signin_btn_icon_disabled_dark = 0x7f020096;
		public static final int common_signin_btn_icon_disabled_focus_dark = 0x7f020097;
		public static final int common_signin_btn_icon_disabled_focus_light = 0x7f020098;
		public static final int common_signin_btn_icon_disabled_light = 0x7f020099;
		public static final int common_signin_btn_icon_focus_dark = 0x7f02009a;
		public static final int common_signin_btn_icon_focus_light = 0x7f02009b;
		public static final int common_signin_btn_icon_light = 0x7f02009c;
		public static final int common_signin_btn_icon_normal_dark = 0x7f02009d;
		public static final int common_signin_btn_icon_normal_light = 0x7f02009e;
		public static final int common_signin_btn_icon_pressed_dark = 0x7f02009f;
		public static final int common_signin_btn_icon_pressed_light = 0x7f0200a0;
		public static final int common_signin_btn_text_dark = 0x7f0200a1;
		public static final int common_signin_btn_text_disabled_dark = 0x7f0200a2;
		public static final int common_signin_btn_text_disabled_focus_dark = 0x7f0200a3;
		public static final int common_signin_btn_text_disabled_focus_light = 0x7f0200a4;
		public static final int common_signin_btn_text_disabled_light = 0x7f0200a5;
		public static final int common_signin_btn_text_focus_dark = 0x7f0200a6;
		public static final int common_signin_btn_text_focus_light = 0x7f0200a7;
		public static final int common_signin_btn_text_light = 0x7f0200a8;
		public static final int common_signin_btn_text_normal_dark = 0x7f0200a9;
		public static final int common_signin_btn_text_normal_light = 0x7f0200aa;
		public static final int common_signin_btn_text_pressed_dark = 0x7f0200ab;
		public static final int common_signin_btn_text_pressed_light = 0x7f0200ac;
		public static final int ic_plusone_medium_off_client = 0x7f020426;
		public static final int ic_plusone_small_off_client = 0x7f020427;
		public static final int ic_plusone_standard_off_client = 0x7f020428;
		public static final int ic_plusone_tall_off_client = 0x7f020429;
		public static final int powered_by_google_dark = 0x7f02046e;
		public static final int powered_by_google_light = 0x7f02046f;
	}
	public static final class id {
		public static final int adjust_height = 0x7f0e0031;
		public static final int adjust_width = 0x7f0e0032;
		public static final int book_now = 0x7f0e005d;
		public static final int buyButton = 0x7f0e005b;
		public static final int buy_now = 0x7f0e005e;
		public static final int buy_with_google = 0x7f0e005f;
		public static final int classic = 0x7f0e0061;
		public static final int donate_with_google = 0x7f0e0060;
		public static final int grayscale = 0x7f0e0062;
		public static final int holo_dark = 0x7f0e0056;
		public static final int holo_light = 0x7f0e0057;
		public static final int hybrid = 0x7f0e0033;
		public static final int match_parent = 0x7f0e0049;
		public static final int monochrome = 0x7f0e0063;
		public static final int none = 0x7f0e0019;
		public static final int normal = 0x7f0e0010;
		public static final int production = 0x7f0e0058;
		public static final int sandbox = 0x7f0e0059;
		public static final int satellite = 0x7f0e0034;
		public static final int selectionDetails = 0x7f0e005c;
		public static final int slide = 0x7f0e001e;
		public static final int strict_sandbox = 0x7f0e005a;
		public static final int terrain = 0x7f0e0035;
		public static final int wrap_content = 0x7f0e004a;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0b0003;
	}
	public static final class raw {
		public static final int gtm_analytics = 0x7f060000;
	}
	public static final class string {
		public static final int accept = 0x7f070057;
		public static final int common_android_wear_notification_needs_update_text = 0x7f07000d;
		public static final int common_android_wear_update_text = 0x7f07000e;
		public static final int common_android_wear_update_title = 0x7f07000f;
		public static final int common_google_play_services_enable_button = 0x7f070010;
		public static final int common_google_play_services_enable_text = 0x7f070011;
		public static final int common_google_play_services_enable_title = 0x7f070012;
		public static final int common_google_play_services_error_notification_requested_by_msg = 0x7f070013;
		public static final int common_google_play_services_install_button = 0x7f070014;
		public static final int common_google_play_services_install_text_phone = 0x7f070015;
		public static final int common_google_play_services_install_text_tablet = 0x7f070016;
		public static final int common_google_play_services_install_title = 0x7f070017;
		public static final int common_google_play_services_invalid_account_text = 0x7f070018;
		public static final int common_google_play_services_invalid_account_title = 0x7f070019;
		public static final int common_google_play_services_needs_enabling_title = 0x7f07001a;
		public static final int common_google_play_services_network_error_text = 0x7f07001b;
		public static final int common_google_play_services_network_error_title = 0x7f07001c;
		public static final int common_google_play_services_notification_needs_installation_title = 0x7f07001d;
		public static final int common_google_play_services_notification_needs_update_title = 0x7f07001e;
		public static final int common_google_play_services_notification_ticker = 0x7f07001f;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f070020;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f070021;
		public static final int common_google_play_services_unknown_issue = 0x7f070022;
		public static final int common_google_play_services_unsupported_text = 0x7f070023;
		public static final int common_google_play_services_unsupported_title = 0x7f070024;
		public static final int common_google_play_services_update_button = 0x7f070025;
		public static final int common_google_play_services_update_text = 0x7f070026;
		public static final int common_google_play_services_update_title = 0x7f070027;
		public static final int common_open_on_phone = 0x7f070028;
		public static final int common_signin_button_text = 0x7f070029;
		public static final int common_signin_button_text_long = 0x7f07002a;
		public static final int commono_google_play_services_api_unavailable_text = 0x7f07002b;
		public static final int create_calendar_message = 0x7f070097;
		public static final int create_calendar_title = 0x7f070098;
		public static final int decline = 0x7f070099;
		public static final int store_picture_message = 0x7f0700d8;
		public static final int store_picture_title = 0x7f0700d9;
		public static final int wallet_buy_button_place_holder = 0x7f070049;
	}
	public static final class style {
		public static final int Theme_IAPTheme = 0x7f09013a;
		public static final int WalletFragmentDefaultButtonTextAppearance = 0x7f090140;
		public static final int WalletFragmentDefaultDetailsHeaderTextAppearance = 0x7f090141;
		public static final int WalletFragmentDefaultDetailsTextAppearance = 0x7f090142;
		public static final int WalletFragmentDefaultStyle = 0x7f090143;
	}
	public static final class styleable {
		public static final int[] AdsAttrs = { 0x7f010035, 0x7f010036, 0x7f010037 };
		public static final int AdsAttrs_adSize = 0;
		public static final int AdsAttrs_adSizes = 1;
		public static final int AdsAttrs_adUnitId = 2;
		public static final int[] CustomWalletTheme = { 0x7f010063 };
		public static final int CustomWalletTheme_windowTransitionStyle = 0;
		public static final int[] LoadingImageView = { 0x7f0100d8, 0x7f0100d9, 0x7f0100da };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
		public static final int[] MapAttrs = { 0x7f0100db, 0x7f0100dc, 0x7f0100dd, 0x7f0100de, 0x7f0100df, 0x7f0100e0, 0x7f0100e1, 0x7f0100e2, 0x7f0100e3, 0x7f0100e4, 0x7f0100e5, 0x7f0100e6, 0x7f0100e7, 0x7f0100e8, 0x7f0100e9, 0x7f0100ea };
		public static final int MapAttrs_cameraBearing = 1;
		public static final int MapAttrs_cameraTargetLat = 2;
		public static final int MapAttrs_cameraTargetLng = 3;
		public static final int MapAttrs_cameraTilt = 4;
		public static final int MapAttrs_cameraZoom = 5;
		public static final int MapAttrs_liteMode = 6;
		public static final int MapAttrs_mapType = 0;
		public static final int MapAttrs_uiCompass = 7;
		public static final int MapAttrs_uiMapToolbar = 15;
		public static final int MapAttrs_uiRotateGestures = 8;
		public static final int MapAttrs_uiScrollGestures = 9;
		public static final int MapAttrs_uiTiltGestures = 10;
		public static final int MapAttrs_uiZoomControls = 11;
		public static final int MapAttrs_uiZoomGestures = 12;
		public static final int MapAttrs_useViewLifecycle = 13;
		public static final int MapAttrs_zOrderOnTop = 14;
		public static final int[] WalletFragmentOptions = { 0x7f010221, 0x7f010222, 0x7f010223, 0x7f010224 };
		public static final int WalletFragmentOptions_appTheme = 0;
		public static final int WalletFragmentOptions_environment = 1;
		public static final int WalletFragmentOptions_fragmentMode = 3;
		public static final int WalletFragmentOptions_fragmentStyle = 2;
		public static final int[] WalletFragmentStyle = { 0x7f010225, 0x7f010226, 0x7f010227, 0x7f010228, 0x7f010229, 0x7f01022a, 0x7f01022b, 0x7f01022c, 0x7f01022d, 0x7f01022e, 0x7f01022f };
		public static final int WalletFragmentStyle_buyButtonAppearance = 3;
		public static final int WalletFragmentStyle_buyButtonHeight = 0;
		public static final int WalletFragmentStyle_buyButtonText = 2;
		public static final int WalletFragmentStyle_buyButtonWidth = 1;
		public static final int WalletFragmentStyle_maskedWalletDetailsBackground = 6;
		public static final int WalletFragmentStyle_maskedWalletDetailsButtonBackground = 8;
		public static final int WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance = 7;
		public static final int WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance = 5;
		public static final int WalletFragmentStyle_maskedWalletDetailsLogoImageType = 10;
		public static final int WalletFragmentStyle_maskedWalletDetailsLogoTextColor = 9;
		public static final int WalletFragmentStyle_maskedWalletDetailsTextAppearance = 4;
	}
}
